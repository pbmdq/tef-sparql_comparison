package ch.uzh.ifi.ddis.ifp.le;

/*
 * #%L
 * Linked Data Esper
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

public class TripleBase {
	private Long _start;
	private Long _end;
	
	public TripleBase(Long start, Long end) {
		
		_start 	= start;
		_end	= end;
			
	}
	
	
	
	public Long getStart() {
		return _start;
	}
	
	 public Long getEnd() {
		return _end;
	}
	
}
