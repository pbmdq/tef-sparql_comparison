package ch.uzh.ifi.ddis.ifp.le;

/*
 * #%L
 * Linked Data Esper
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

public class TripleBasicElement {
	public String _stringValue;
	public double _doubleValue;
	
	public TripleBasicElement(String stringValue, double doubleValue) {
		_stringValue 	= stringValue;
		_doubleValue	= doubleValue;
	}
	
	public String getStringValue(){
		return _stringValue;
	}
	public double getdoubleValue(){
		return _doubleValue;
	}
	@Override
	public String toString(){
		return _stringValue;
	}
	
	
}
