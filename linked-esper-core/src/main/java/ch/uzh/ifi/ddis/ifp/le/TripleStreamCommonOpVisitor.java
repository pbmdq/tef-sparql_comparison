package ch.uzh.ifi.ddis.ifp.le;

/*
 * #%L
 * Linked Data Esper
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.ddis.ifp.algebra.BasicGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.ColumnListNode;
import ch.uzh.ifi.ddis.ifp.algebra.ExpressionNode;
import ch.uzh.ifi.ddis.ifp.algebra.FactOp;
import ch.uzh.ifi.ddis.ifp.algebra.FilterOp;
import ch.uzh.ifi.ddis.ifp.algebra.GroupGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.Node;
import ch.uzh.ifi.ddis.ifp.algebra.Op;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;
import ch.uzh.ifi.ddis.ifp.algebra.ProjectionOp;
import ch.uzh.ifi.ddis.ifp.algebra.TriplePattern;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;
import ch.uzh.ifi.ddis.ifp.algebra.impl.GroupByClause;
import ch.uzh.ifi.ddis.ifp.algebra.impl.ListClause;
import ch.uzh.ifi.ddis.ifp.algebra.impl.ReplaceOnOpImpl;
import ch.uzh.ifi.ddis.ifp.algebra.impl.SinceUntilOpImpl;

import com.espertech.esper.client.ConfigurationOperations;
import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EventType;
import com.espertech.esper.client.soda.ArithmaticExpression;
import com.espertech.esper.client.soda.Assignment;
import com.espertech.esper.client.soda.CompareListExpression;
import com.espertech.esper.client.soda.Conjunction;
import com.espertech.esper.client.soda.ConstantExpression;
import com.espertech.esper.client.soda.CreateWindowClause;
import com.espertech.esper.client.soda.EPStatementObjectModel;
import com.espertech.esper.client.soda.Expression;
import com.espertech.esper.client.soda.Expressions;
import com.espertech.esper.client.soda.Filter;
import com.espertech.esper.client.soda.FilterStream;
import com.espertech.esper.client.soda.OnMergeClause;
import com.espertech.esper.client.soda.OnMergeMatchItem;
import com.espertech.esper.client.soda.OnMergeMatchedAction;
import com.espertech.esper.client.soda.OnMergeMatchedInsertAction;
import com.espertech.esper.client.soda.OnMergeMatchedUpdateAction;
import com.espertech.esper.client.soda.SchemaColumnDesc;
import com.espertech.esper.client.soda.Stream;
import com.espertech.esper.client.soda.FromClause;
import com.espertech.esper.client.soda.InsertIntoClause;
import com.espertech.esper.client.soda.MaxRowExpression;
import com.espertech.esper.client.soda.MinRowExpression;
import com.espertech.esper.client.soda.PropertyValueExpression;
//import com.espertech.esper.client.soda.PropertyValueExpressionPair;
import com.espertech.esper.client.soda.RelationalOpExpression;
import com.espertech.esper.client.soda.SelectClause;
import com.espertech.esper.client.soda.SelectClauseElement;
import com.espertech.esper.client.soda.SelectClauseExpression;
import com.espertech.esper.client.soda.StreamSelector;
import com.espertech.esper.client.soda.View;

/**
 * <p>
 * Visitor pattern for streams of time annotated triple patterns.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class TripleStreamCommonOpVisitor implements
		OpVisitorReturn<TripleStreamCommonOpVisitor.QueryAndTypeMap> {

	private static final Logger _log = LoggerFactory
			.getLogger(TripleStreamCommonOpVisitor.class);

	private final String _eventTypeName;

	private final ConfigurationOperations _esperConfig;

	private final EPAdministrator _esperAdmin;

	public static final String SELECT_SUBJECT = "subject";
	public static final String SELECT_PREDICATE = "testtest";
	public static final String SELECT_OBJECT = "object";

	/**
	 * <p>
	 * </p>
	 * 
	 * @param epAdministrator
	 * @param eventTypeName
	 *            name of the incoming data stream.
	 */
	public TripleStreamCommonOpVisitor(EPAdministrator epAdministrator,
			String eventTypeName) {
		if (epAdministrator == null) {
			throw new NullPointerException();
		}
		if (eventTypeName == null) {
			throw new NullPointerException();
		}
		_esperConfig = epAdministrator.getConfiguration();

		String[] test = _esperConfig.getEventType(eventTypeName)
				.getPropertyNames();
		System.out.println(test.length);

		_eventTypeName = eventTypeName;
		_esperAdmin = epAdministrator;

		if (_esperConfig.getEventType(eventTypeName) == null) {
			_esperConfig.getEventType(eventTypeName).getPropertyNames();
			throw new IllegalArgumentException();
		}
		// Class test1 =
		// _esperConfig.getEventType(eventTypeName).getPropertyType("predicate");//.getPropertyNames();
		// System.out.println(test1);
	}

	/**
	 * <p>
	 * Remove all but the specified properties from the SELECT clause of the
	 * {@link Op}. Projection to properties not contained in the SELECT clause
	 * are ignored.
	 * </p>
	 * *
	 * <p>
	 * This method adds no query for the {@link Op} to the
	 * {@link EPAdministrator} and creates no {@link EventType} objects.
	 * </p>
	 */
	@Override
	public QueryAndTypeMap visit(ProjectionOp projectionOp) {
		QueryAndTypeMap childrenResult = (QueryAndTypeMap) projectionOp.getOp().visit(
				this);
		EPStatementObjectModel projectQuery = new EPStatementObjectModel();
		String tempID = projectionOp.getOp().getId().toString();

		final Set<String> projectionProperties = projectionOp.getProperties();
		final Map<String, Object> resultProjectionProperties = new HashMap<String, Object>();
		if (!projectionProperties.contains("start")) {
			projectionProperties.add("start");
		}
		if (!projectionProperties.contains("end")) {
			projectionProperties.add("end");
		}

		List<SelectClauseElement> selectListNew = new ArrayList<SelectClauseElement>();
		for (SelectClauseElement selectTerm : childrenResult.query.getSelectClause()
				.getSelectList()) {
			if (selectTerm instanceof SelectClauseExpression) {
				final SelectClauseExpression sce = (SelectClauseExpression) selectTerm;
				if (!projectionProperties.contains(sce.getAsName())) {
				} else {
					resultProjectionProperties.put(sce.getAsName(),
							Object.class);
					selectListNew.add(sce);
					// TODO may be wrong here
				}
			} else {
				throw new IllegalArgumentException();
			}
		}
		SelectClause selectClauseNew = null;
		

			selectClauseNew = SelectClause.create();
			
			for (SelectClauseElement selectTerm : selectListNew) {
				SelectClauseExpression sce = (SelectClauseExpression) selectTerm;
				_log.debug("add select clause '{}' '{}' '{}'", sce.getAsName(),
						tempID, selectListNew.size());
				selectClauseNew.addWithAsProvidedName(
						String.format("%s.%s", tempID, sce.getAsName()),
						sce.getAsName());
			}
		
		projectQuery.setSelectClause(selectClauseNew);
		
		projectQuery.setFromClause(FromClause.create(FilterStream
				.create(tempID)));
		
		projectQuery.setWhereClause(null);
		
		//setGourpByClause(result.query, projectionOp.getGroupByClause(),
		//		resultProjectionProperties);
		
		_log.debug("Creating new query in proj end: '{}'", projectQuery.toEPL());

		QueryAndTypeMap tempResult = new QueryAndTypeMap(projectQuery,
				resultProjectionProperties);

		String outputCache = projectionOp.getOutputCache() == null ? projectionOp
				.getId().toString() : projectionOp.getOutputCache();
		putInsertQuerytoEngine(outputCache, tempResult);
		return tempResult;

	}

	/**
	 * <p>
	 * Create a single new query for this group graph pattern that consumes from
	 * its components. In that sense, all bgps and ggps that are components of
	 * this query are treated as separate queries.
	 * </p>
	 * <p>
	 * This method adds the queries for the sub-components to the
	 * {@link EPAdministrator} and creates the necessary {@link EventType}
	 * objects.
	 * </p>
	 */
	@Override
	public QueryAndTypeMap visit(GroupGraphPatternOp graphPatternOp) {
		final EPStatementObjectModel graphPatternQuery = new EPStatementObjectModel();
		final SelectClause graphPatternSelectClause = SelectClause.create();
		final FromClause graphPatternFromClause = FromClause.create();
		final MinRowExpression minRowStartExpr = new MinRowExpression();
		final MaxRowExpression maxRowEndExpr = new MaxRowExpression();

		minRowStartExpr.add(Long.MAX_VALUE);
		maxRowEndExpr.add(0);

		final Map<String, Object> graphPatternProperties = new HashMap<String, Object>();
		for (GroupGraphPatternOp ggpOp : graphPatternOp.getSubGraphPatterns()) {
			final QueryAndTypeMap qatm = (QueryAndTypeMap) ggpOp.visit(this);
			final String ggpId = ggpOp.getId().toString();

			// Add properties not yet contained to SELECT clause
			for (Entry<String, Object> propertytoSelectEntry : qatm.properties
					.entrySet()) {
				final String key = propertytoSelectEntry.getKey();
				final Object value = propertytoSelectEntry.getValue();
				if (!"start".equals(key) && !"end".equals(key)
						&& !graphPatternProperties.containsKey(key)) {
					graphPatternSelectClause.addWithAsProvidedName(
							String.format("%s.%s", ggpId, key), key);

					graphPatternProperties.put(key, value);
				}
			}
			// TODO implement filter conditions
			final Filter filter = new Filter();
			graphPatternFromClause.add(new FilterStream(filter, ggpId));

			// Handle timestamps
			minRowStartExpr.add(String.format("%s.start", ggpId));
			maxRowEndExpr.add(String.format("%s.end", ggpId));

		}
		for (BasicGraphPatternOp bgpOp : graphPatternOp.getBasicGraphPatterns()) {
			final QueryAndTypeMap qatm = (QueryAndTypeMap) bgpOp.visit(this);
			final String bgpId = bgpOp.getId().toString();
			// Add properties not yet contained to SELECT clause
			for (Entry<String, Object> propertytoSelectEntry : qatm.properties
					.entrySet()) {
				final String key = propertytoSelectEntry.getKey();
				final Object value = propertytoSelectEntry.getValue();
				if (!"start".equals(key) && !"end".equals(key)
						&& !graphPatternProperties.containsKey(key)) {
					graphPatternSelectClause.addWithAsProvidedName(
							String.format("%s.%s", bgpId, key), key);

					graphPatternProperties.put(key, value);
				}
			}

			// TODO implement filter conditions
			final Filter filter = new Filter(bgpId);
			// graphPatternFromClause.add(new FilterStream(filter));

			// Handle timestamps
			minRowStartExpr.add(String.format("%s.start", bgpId));
			maxRowEndExpr.add(String.format("%s.end", bgpId));

			/*
			 * View view; if (basicGraphPatternOp.getWindow().getValue() !=
			 * null) view = View.create( "win",
			 * basicGraphPatternOp.getWindow().getType(), new
			 * ConstantExpression(Integer
			 * .parseInt(basicGraphPatternOp.getWindow() .getValue()))); else
			 */
			Expression tpFilter = null;
			View view;
			view = View.create("win", "keepall");
			graphPatternFromClause.add(FilterStream.create(
					bgpOp.getId().toString(), bgpId, tpFilter).addView(view));

		}
		// Filters have only an influence on the pre-filter or the SELECT
		// clause.
		// TODO implement this

		graphPatternSelectClause.add(minRowStartExpr, "start");
		graphPatternSelectClause.add(maxRowEndExpr, "end");
		graphPatternProperties.put("start", Long.class);
		graphPatternProperties.put("end", Long.class);

		graphPatternQuery.setFromClause(graphPatternFromClause);
		graphPatternQuery.setSelectClause(graphPatternSelectClause);
		setWhereClause(graphPatternQuery, graphPatternOp.getCommonVariable());
		
		setGourpByClause(graphPatternQuery, graphPatternOp.getGroupByClause(),
				graphPatternProperties);
		
		if (_log.isDebugEnabled()) {
			_log.debug("Creating new query ggp: '{}'",
					graphPatternQuery.toEPL());
		}

		QueryAndTypeMap tempReturn = new QueryAndTypeMap(graphPatternQuery,
				graphPatternProperties);
		//setGourpByClause(graphPatternQuery, graphPatternOp.getGroupByClause(),
		//		graphPatternProperties);

		String outputCache = graphPatternOp.getOutputCache() == null ? graphPatternOp
				.getId().toString() : graphPatternOp.getOutputCache();
		putInsertQuerytoEngine(outputCache, tempReturn);

		return tempReturn;
	}

	@Override
	public QueryAndTypeMap visit(FilterOp filterOp) {
		Filter filter = new Filter();
		// filter.setFilter();
		return null;
	}

	/**
	 * <p>
	 * For each triple pattern, we add a new {@link Stream} to the
	 * {@link FromClause}. No optimization takes place at this stage.
	 * </p>
	 * <p>
	 * This method does not modify the Esper {@link ConfigurationOperations}.
	 * </p>
	 */
	private void addFilterExpression(Map<String, WhereNode> filterVariables,
			String variable, String variableFullName,
			List<Expression> tpFilterChildren) {
		if (filterVariables.containsKey(variable)) {
			WhereNode temp = filterVariables.get(variable);
			RelationalOpExpression exp = new RelationalOpExpression(
					new PropertyValueExpression(variableFullName),
					temp.getOperator(),
					new ConstantExpression(temp.getRightP()));
			tpFilterChildren.add(exp);
		}
	}

	@Override
	public QueryAndTypeMap visit(BasicGraphPatternOp basicGraphPatternOp) {
		if (basicGraphPatternOp.getOutputCache() != null)
			_log.debug("output cache is: '{}'",
					basicGraphPatternOp.getOutputCache());

		final EPStatementObjectModel bgpQuery = new EPStatementObjectModel();
		final SelectClause bgpSelectClause = SelectClause.create();
		final FromClause bgpFromClause = FromClause.create();

		final MinRowExpression minRowStartExpr = new MinRowExpression();
		final MaxRowExpression maxRowEndExpr = new MaxRowExpression();
		// final Expression whereClause = new Expression();
		
		minRowStartExpr.add(0);
		maxRowEndExpr.add(0);

		final Map<String, Object> bgpProperties = new HashMap<String, Object>();
		int counter = 0;
		final Set<String> variableNames = new LinkedHashSet<String>();

		final Map<String, WhereNode> filterVariables = new HashMap<String, WhereNode>();
		for (WhereNode filterNode : basicGraphPatternOp.getFilters()) {
			filterVariables.put(filterNode.getLeftP(), filterNode);
			_log.debug("Creating new filter: '{}'", filterNode.getLeftP());
		}

		for (TriplePattern tp : basicGraphPatternOp.getTriplePatterns()) {

			final Node subject = tp.getSubject();
			final Node predicate = tp.getPredicate();
			final Node object = tp.getObject();
			final VariableNode subjectVar = subject.isVariable() ? subject
					.asVariable() : null;
			final VariableNode predicateVar = predicate.isVariable() ? predicate
					.asVariable() : null;
			final VariableNode objectVar = object.isVariable() ? object
					.asVariable() : null;
			final String resultEventTypeName = String.format("tp_%s", counter);
			tp.setResultEventName(resultEventTypeName);
			++counter;

			minRowStartExpr.add(String.format("%s.start", resultEventTypeName));
			maxRowEndExpr.add(String.format("%s.end", resultEventTypeName));

			if (subjectVar != null) {
				bgpProperties.put(subjectVar.toString(), Object.class);
			}
			if (predicateVar != null) {
				bgpProperties.put(predicateVar.toString(), Object.class);
			}
			if (objectVar != null) {
				bgpProperties.put(objectVar.toString(), Object.class);
			}
			List<Expression> tpFilterChildren = new ArrayList<Expression>();

			// s
			if (subject.isVariable()) {
				if (!variableNames.contains(subjectVar.getName())) {
					variableNames.add(subjectVar.getName());
					// bgpSelectClause.addWithAsProvidedName(
					// subjectVar.getFullRDFName(),
					// subjectVar.asVariable().getName());
					PropertyValueExpression temp = new PropertyValueExpression(
							subjectVar.getFullRDFName());
					bgpSelectClause
							.add(temp, subjectVar.asVariable().getName());

					// bgpSelectClause.
					// ? add double??

				}
				addFilterExpression(filterVariables, subjectVar.toString(),
						String.format("%s.%s", resultEventTypeName,
								SELECT_SUBJECT), tpFilterChildren);
			} else {
				final CompareListExpression exp = new CompareListExpression(
						false, "=");
				exp.addChild(new PropertyValueExpression("subject.stringValue"));
				exp.addChild(new ConstantExpression(tp.getSubject().toString()));
				tpFilterChildren.add(exp);
			}
			// p
			if (predicate.isVariable()) {
				if (!variableNames.contains(predicateVar.getName())) {
					variableNames.add(predicateVar.getName());
					bgpSelectClause.addWithAsProvidedName(predicateVar
							.getFullRDFName(), predicateVar.asVariable()
							.getName());
				}
				addFilterExpression(filterVariables, predicateVar.toString(),
						String.format("%s.%s", resultEventTypeName,
								SELECT_PREDICATE), tpFilterChildren);
			} else {
				final CompareListExpression exp = new CompareListExpression(
						false, "=");
				exp.addChild(new PropertyValueExpression(
						"predicate.stringValue"));
				exp.addChild(new ConstantExpression(tp.getPredicate()
						.toString()));
				tpFilterChildren.add(exp);
			}
			// o
			if (object.isVariable()) {
				if (!variableNames.contains(objectVar.getName())) {
					variableNames.add(objectVar.getName());
					bgpSelectClause
							.addWithAsProvidedName(objectVar.getFullRDFName(),
									objectVar.asVariable().getName());
				}
				addFilterExpression(filterVariables, objectVar.toString(),
						String.format("%s.%s", resultEventTypeName,
								SELECT_OBJECT), tpFilterChildren);
			} else {
				final CompareListExpression exp = new CompareListExpression(
						false, "=");
				exp.addChild(new PropertyValueExpression("object.stringValue"));
				exp.addChild(new ConstantExpression(tp.getObject().toString()));
				tpFilterChildren.add(exp);
			}

			Expression tpFilter = null;
			if (tpFilterChildren.size() > 1) {
				tpFilter = new Conjunction();
				tpFilter.setChildren(tpFilterChildren);
			} else {
				tpFilter = tpFilterChildren.get(0);
			}

			View view;
			if (basicGraphPatternOp.getWindow().getValue() != null) {
				view = View.create("win", basicGraphPatternOp.getWindow()
						.getType(), Expressions.timePeriod(null, null, null, Integer.parseInt(basicGraphPatternOp.getWindow().getValue()), null));
				//view = View.create("win", "time_batch", Expressions.timePeriod(null, null, null, Integer.parseInt(basicGraphPatternOp.getWindow().getValue()), null));
				//view = View.create("win", "time", Expressions.timePeriod(null, null, null, null, 10));
				//view = View.create("win", "length", Expressions.constant(2));
				//Expressions.eq(left, right)
			} else
				view = View.create("win", "keepall");

			bgpFromClause.add(FilterStream.create(_eventTypeName,
					resultEventTypeName, tpFilter).addView(view));
			}

		bgpSelectClause.add(minRowStartExpr, "start");
		bgpSelectClause.add(maxRowEndExpr, "end");
		
		bgpProperties.put("start", Long.class);
		bgpProperties.put("end", Long.class);
		
		bgpQuery.setFromClause(bgpFromClause);
		bgpQuery.setSelectClause(bgpSelectClause);
		//bgpQuery.setSelectClause(SelectClause.create().add(Expressions.count(Expressions.property("subject"))));
		StreamSelector streamSelector = StreamSelector.RSTREAM_ISTREAM_BOTH;
		bgpQuery.getSelectClause().setStreamSelector(streamSelector);
				
		setWhereClause(bgpQuery, basicGraphPatternOp.getCommonVariable());
		// bgpQuery.setWhereClause(bgpWhereClause);
		if (_log.isDebugEnabled()) {
			_log.debug("Creating new query bgp: '{}'", bgpQuery.toEPL());
		}
		QueryAndTypeMap tempReturn = new QueryAndTypeMap(bgpQuery,
				bgpProperties);

		String outputCache = basicGraphPatternOp.getOutputCache() == null ? basicGraphPatternOp
				.getId().toString() : basicGraphPatternOp.getOutputCache();

		// ((SelectClauseExpression)(tempReturn.query.getSelectClause().getSelectList().get(0))).;
		/*
		 * _log.debug("Adding new insert into as intermediate queue: '{}'",
		 * outputCache); _esperConfig.addEventType(outputCache, bgpProperties);
		 * InsertIntoClause insertInto = InsertIntoClause.create(outputCache);
		 * tempReturn.query.setInsertInto(insertInto);
		 * _esperAdmin.create(tempReturn.query);
		 */
		setGourpByClause(bgpQuery, basicGraphPatternOp.getGroupByClause(),
				bgpProperties);

		// put into an insert

		putInsertQuerytoEngine(outputCache, tempReturn);

		return tempReturn;
	}

	/**
	 * <p>
	 * help function, put intert query into Esper Engine this function is in the
	 * end of each operator
	 * </p>
	 * 
	 * @author Shen Gao
	 * 
	 * 
	 */
	public void putInsertQuerytoEngine(String id, QueryAndTypeMap mappedResult) {
		// common stuff
		StreamSelector streamSelector = StreamSelector.RSTREAM_ISTREAM_BOTH;
		if(mappedResult.query.getSelectClause() != null)
			mappedResult.query.getSelectClause().setStreamSelector(streamSelector);
		
		_log.debug("Adding new insert into as intermediate queue: '{}'", id);
		_esperConfig.addEventType(id, mappedResult.properties);
		InsertIntoClause insertInto = InsertIntoClause.create(id);
		mappedResult.query.setInsertInto(insertInto);
		_esperAdmin.create(mappedResult.query);
	}

	/**
	 * <p>
	 * Gourp by clause
	 * </p>
	 * 
	 * @author Shen Gao
	 * 
	 *         tripple pattern pointing to itself?
	 */
	public void setGourpByClause(EPStatementObjectModel resultQuery,
			GroupByClause inputGroupByClauses, Map<String, Object> properties) {
		if (inputGroupByClauses != null) {
			List<Expression> groupByExpressions = new ArrayList<Expression>();
			for (Node basicNode : inputGroupByClauses.getGroupByAttributes()) {
				for (SelectClauseElement tempSelect : resultQuery
						.getSelectClause().getSelectList()) {
					// System.out.println(((SelectClauseExpression)
					// tempSelect).getAsName());
					// System.out.println(basicNode.asVariable().getFullName());

					if (((SelectClauseExpression) tempSelect).getAsName()
							.equalsIgnoreCase(
									basicNode.asVariable().getFullName())) {
						groupByExpressions
								.add(((SelectClauseExpression) tempSelect)
										.getExpression());
					}
				}
			}
			com.espertech.esper.client.soda.GroupByClause groupByClause = com.espertech.esper.client.soda.GroupByClause
					.create();
			groupByClause.setGroupByExpressions(groupByExpressions);
			resultQuery.setGroupByClause(groupByClause);

			if (inputGroupByClauses.getHavingClauses().size() > 1) {
				Conjunction conjunctionHavingClause = new Conjunction();
				for (ListClause comparisionClause : inputGroupByClauses
						.getHavingClauses()) {

					/*
					 * RelationalOpExpression havingExpression = new
					 * RelationalOpExpression( new
					 * PropertyValueExpression(comparisionClause
					 * .getLeftNode().getFullName()),
					 * comparisionClause.getComparisionOperator(), new
					 * PropertyValueExpression
					 * (comparisionClause.getRigthNode().getFullName()));
					 * conjunctionHavingClause.add(havingExpression);
					 */
				}
				resultQuery.setHavingClause(conjunctionHavingClause);
			} else if (inputGroupByClauses.getHavingClauses().size() == 1) {
				for (ListClause comparisionClause : inputGroupByClauses
						.getHavingClauses()) {
					resultQuery.setHavingClause(comparisionClause
							.toExpression(resultQuery));
				}

			}

		}
	}

	/**
	 * <p>
	 * help function, get common variable
	 * </p>
	 * 
	 * @author Shen Gao
	 * 
	 *         tripple pattern pointing to itself?
	 */
	public void setWhereClause(EPStatementObjectModel graphPatternOp,
			Map<String, Collection<String>> commonVariables) {
		ArrayList<RelationalOpExpression> whereClause = new ArrayList<RelationalOpExpression>();
		for (Map.Entry<String, Collection<String>> entry : commonVariables
				.entrySet()) {
			if (entry.getValue().size() > 1) {
				String tempString = null;
				for (String variable : entry.getValue()) {
					if (tempString != null) {
						whereClause.add(Expressions.eqProperty(tempString
								+ ".stringValue", variable + ".stringValue"));
						// whereClause.add(Expressions.eqProperty("tp_0.subject.stringValue",
						// "tp_1.subject.stringValue"));
						// System.out.println(tempString);
					}
					tempString = variable;
				}
			}
		}
		if (whereClause.size() > 1) {
			final Conjunction conjunctionWhereClause = new Conjunction();
			for (int i = 0; i < whereClause.size(); i++) {
				conjunctionWhereClause.add(whereClause.get(i));
			}
			graphPatternOp.setWhereClause(conjunctionWhereClause);
		} else if (whereClause.size() == 1)
			graphPatternOp.setWhereClause(whereClause.get(0));

	}

	/**
	 * <p>
	 * Create a named window in esper a fact have three components since, set
	 * the S P O and start until, update the end S P O replace on, update the
	 * </p>
	 */
	/*
	 * create window query
	 */
	private void createWindowQuery(FactOp factOp) {
		final EPStatementObjectModel createWindowQery = new EPStatementObjectModel();
		String windowName = factOp.getFactName();
		CreateWindowClause createWindow = CreateWindowClause.create(windowName,
				View.create("win", "keepall"));
		createWindowQery.setCreateWindow(createWindow);
		_log.debug("creating window query'{}'", createWindowQery.toEPL()
				.toString());
		List<SchemaColumnDesc> columns = new ArrayList<SchemaColumnDesc>();
		for (ColumnListNode tempColumn : factOp.getFactColumns()) {
			columns.add(new SchemaColumnDesc(tempColumn.getColumnName(),
					tempColumn.getColumnType(), false));
		}
		createWindow.setColumns(columns);
		_esperAdmin.create(createWindowQery);
		_log.debug("creating window query'{}'", createWindowQery.toEPL()
				.toString());
	}

	Expression getRightExpression(ExpressionNode expressionNode) {
		/*
		 * if (expressionNode.getOperator() == null) return new
		 * ConstantExpression(expressionNode.getLeftNode() .getFullName()); else
		 * { // TODO type now default is integer?
		 * _log.debug("checkinhg right node'{}'", expressionNode
		 * .getRightNode().getFullName()); return new ArithmaticExpression(new
		 * PropertyValueExpression( expressionNode.getLeftNode().getFullName()),
		 * expressionNode.getOperator(), new ConstantExpression(
		 * Integer.parseInt(expressionNode.getRightNode() .getFullName()))); }
		 */
		return null;
	}

	List<Assignment> composeAssignments(Set<ExpressionNode> expressionNodes) {
		List<Assignment> assignments = new ArrayList<Assignment>();
		for (ExpressionNode tempNode : expressionNodes) {
			RelationalOpExpression updateExpression = new RelationalOpExpression(
					new PropertyValueExpression(tempNode.asVariable()
							.getFullName()), "=",
					getRightExpression(tempNode.getRightNode()));
			assignments.add(new Assignment(updateExpression));
		}
		return assignments;
	}

	private void createSinceUntilQuery(FactOp factOp) {
		// create since, unitll and replace on
		for (SinceUntilOpImpl sinceUntil : factOp.getSinceUntilOps()) {
			final EPStatementObjectModel factQeryOnQuery = new EPStatementObjectModel();
			String windowName = factOp.getFactName();
			OnMergeClause onMergeSince = new OnMergeClause();
			List<OnMergeMatchItem> onMergeMatchItems = new ArrayList<OnMergeMatchItem>();

			onMergeSince.setWindowName(windowName);
			onMergeSince.setOptionalAsName(factOp.getId().toString());
			FromClause fromClause = null;
			_log.debug("checking perportie length'{}'", _esperConfig
					.getEventType(sinceUntil.getOnEventName())
					.getPropertyNames().length);
			// where clause
			List<RelationalOpExpression> whereClauses = new ArrayList<RelationalOpExpression>();
			for (ListClause where : sinceUntil.getWhereClauses()) {
				whereClauses.add((RelationalOpExpression) where.toExpression());
			}
			if (whereClauses.size() > 1) {
				final Conjunction conjunctionWhereClause = new Conjunction();
				for (int i = 0; i < whereClauses.size(); i++) {
					conjunctionWhereClause.add(whereClauses.get(i));
				}
				factQeryOnQuery.setWhereClause(conjunctionWhereClause);
			} else if (whereClauses.size() == 1)
				factQeryOnQuery.setWhereClause(whereClauses.get(0));

			// Since = on merge insert
			if (sinceUntil.getType().equals("SINCE")) {
				OnMergeMatchedInsertAction onMergeInsert = new OnMergeMatchedInsertAction();
				SelectClause selectClauseNew = SelectClause.create();

				selectClauseNew.addWildcard();
				onMergeInsert.setSelectList(selectClauseNew.getSelectList());
				List<OnMergeMatchedAction> onMergeActionsNotMatched = new ArrayList<OnMergeMatchedAction>();
				onMergeActionsNotMatched.add(onMergeInsert);
				onMergeMatchItems.add(new OnMergeMatchItem(false, null,
						onMergeActionsNotMatched));
			} else if (sinceUntil.getType().equals("UNTIL")) {
				OnMergeMatchedUpdateAction onMergeUpdate = new OnMergeMatchedUpdateAction();
				List<Assignment> assignments = new ArrayList<Assignment>();

				RelationalOpExpression updateExpression = new RelationalOpExpression(
						new PropertyValueExpression(factOp.getFactName()
								+ ".end"), "=", Expressions.cast(new PropertyValueExpression(sinceUntil.getOnEventName() + ".end"), "Long"));
				// new PropertyValueExpression(sinceUntil.getOnEventName() + ".end")
				assignments.add(new Assignment(updateExpression));
				
				onMergeUpdate.setAssignments(assignments);

				List<OnMergeMatchedAction> onMergeActionsMatched = new ArrayList<OnMergeMatchedAction>();
				onMergeActionsMatched.add(onMergeUpdate);
				onMergeMatchItems.add(new OnMergeMatchItem(true, null,
						onMergeActionsMatched));
			} else if (sinceUntil.getType().equals("REPLACEON")) {

				OnMergeMatchedUpdateAction onMergeUpdate = new OnMergeMatchedUpdateAction();
				((ReplaceOnOpImpl) sinceUntil).getSetClauses();
				List<Assignment> assignments = new ArrayList<Assignment>();
				
				for (ListClause setClause : ((ReplaceOnOpImpl) sinceUntil).getSetClauses()) {
					assignments.add(new Assignment(setClause.toExpression( true )));
				}
				
				//onMergeUpdate.
				onMergeUpdate.setAssignments(assignments);
				List<OnMergeMatchedAction> onMergeActionsMatched = new ArrayList<OnMergeMatchedAction>();
				onMergeActionsMatched.add(onMergeUpdate);
				onMergeMatchItems.add(new OnMergeMatchItem(true, null,
						onMergeActionsMatched));
			}

			fromClause = FromClause.create(FilterStream.create(sinceUntil
					.getOnEventName()));
			factQeryOnQuery.setFromClause(fromClause);

			factQeryOnQuery.setOnExpr(OnMergeClause.create(windowName,
					windowName, onMergeMatchItems));
			_log.debug("Compiling Since query'{}'", factQeryOnQuery.toEPL()
					.toString());
			_esperAdmin.create(factQeryOnQuery);
		}
	}

	private void createReplaceOnQuery(FactOp factOp) {
		// create since, unitll and replace on
		for (ReplaceOnOpImpl sinceUntil : factOp.getReplaceOnOps()) {
			final EPStatementObjectModel factQeryOnQuery = new EPStatementObjectModel();
			String windowName = factOp.getFactName();
			OnMergeClause onMergeSince = new OnMergeClause();
			List<OnMergeMatchItem> onMergeMatchItems = new ArrayList<OnMergeMatchItem>();

			onMergeSince.setWindowName(windowName);
			onMergeSince.setOptionalAsName(factOp.getId().toString());
			FromClause fromClause = null;
			_log.debug("checkinhg perportie length'{}'", _esperConfig
					.getEventType(sinceUntil.getOnEventName())
					.getPropertyNames().length);
			// where clause
			List<RelationalOpExpression> whereClauses = new ArrayList<RelationalOpExpression>();
			for (ListClause where : sinceUntil.getWhereClauses()) {
				whereClauses.add((RelationalOpExpression) where.toExpression());
			}
			if (whereClauses.size() > 1) {
				final Conjunction conjunctionWhereClause = new Conjunction();
				for (int i = 0; i < whereClauses.size(); i++) {
					conjunctionWhereClause.add(whereClauses.get(i));
				}
				factQeryOnQuery.setWhereClause(conjunctionWhereClause);
			} else if (whereClauses.size() == 1)
				factQeryOnQuery.setWhereClause(whereClauses.get(0));

			// Since = on merge insert
			if (sinceUntil.getType().equals("UREPLACEON")) {
				OnMergeMatchedUpdateAction onMergeUpdate = new OnMergeMatchedUpdateAction();
				List<Assignment> assignments = new ArrayList<Assignment>();

				RelationalOpExpression updateExpression = new RelationalOpExpression(
						new PropertyValueExpression(factOp.getFactName()
								+ ".end"), "=", new PropertyValueExpression(
								sinceUntil.getOnEventName() + ".end"));
				assignments.add(new Assignment(updateExpression));
				onMergeUpdate.setAssignments(assignments);

				List<OnMergeMatchedAction> onMergeActionsMatched = new ArrayList<OnMergeMatchedAction>();
				onMergeActionsMatched.add(onMergeUpdate);
				onMergeMatchItems.add(new OnMergeMatchItem(true, null,
						onMergeActionsMatched));
			}

			fromClause = FromClause.create(FilterStream.create(sinceUntil
					.getOnEventName()));
			factQeryOnQuery.setFromClause(fromClause);

			factQeryOnQuery.setOnExpr(OnMergeClause.create(windowName,
					windowName, onMergeMatchItems));
			_log.debug("Compiling Since query'{}'", factQeryOnQuery.toEPL()
					.toString());
			_esperAdmin.create(factQeryOnQuery);
		}
	}

	public QueryAndTypeMap visit(FactOp factOp) {
		// String windowName = factOp.getFactName();
		if (!factOp.getFactColumns().isEmpty())
			createWindowQuery(factOp);
		if (!factOp.getSinceUntilOps().isEmpty())
			createSinceUntilQuery(factOp);
		if (!factOp.getReplaceOnOps().isEmpty())
			createReplaceOnQuery(factOp);
		return new QueryAndTypeMap(null, null);
	}

	/**
	 * <p>
	 * Auxiliary class.
	 * </p>
	 * 
	 * @author Thomas Scharrenbach
	 * 
	 */
	public class QueryAndTypeMap {
		public final EPStatementObjectModel query;

		public final Map<String, Object> properties;

		public QueryAndTypeMap(EPStatementObjectModel query,
				Map<String, Object> properties) {
			this.query = query;
			this.properties = properties;
		}
	}
}
