package ch.uzh.ifi.ddis.ifp.le;

import javax.xml.bind.DatatypeConverter;

import com.espertech.esper.event.xml.XSDSchemaMapper;

import ch.uzh.ifi.ddis.ifp.algebra.impl.Utilities;

/*
 * #%L
 * Linked Data Esper
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

public class TripleNestedType extends TripleBase{

	private TripleBasicElement _subject;

	private TripleBasicElement _predicate;

	private TripleBasicElement _object;
	
	public TripleNestedType(String sub, String predicate, String obj, Long start, Long end) {
		super(start, end);
		_subject 	= new TripleBasicElement(sub, 0.0);
		_predicate 	= new TripleBasicElement(predicate, 0.0);
		double objDouble = 0.0;
		//com.sun.org.apache.xerces.internal.xs		
		
		
		if(obj.contains("float")) {
			objDouble = Utilities.parserXSDDouble(obj);
		}
		_object		= new TripleBasicElement(obj, objDouble);
	}
	
	public TripleBasicElement getSubject() {
		return _subject;
	}
		
	public TripleBasicElement getPredicate() {
		return _predicate;
	}
	
	public TripleBasicElement getObject() {
		return _object;
	}
	

}
