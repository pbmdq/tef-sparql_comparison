package ch.uzh.ifi.ddis.ifp.le;

/*
 * #%L
 * Linked Data Esper
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

import java.net.URI;
import java.util.Random;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.NodeFactory;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;
import ch.uzh.ifi.ddis.ifp.algebra.impl.NodeFactoryImpl;

/**
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class Utils {

	private static final NodeFactory _factory = new NodeFactoryImpl();

	private static final String _iriBase = "http://www.example.com/";

	private static final char[] variableCharacters = { 'a', 'b', 'c', 'd' };

	private static int[] variableCharIndex = new int[5];

	public static BlankNode createRandomBlankNode() {
		return _factory.createBlankNode();
	}

	public static IRINode createRandomIRINode() {
		return _factory.createIRINode(URI.create(_iriBase
				+ new Random().nextInt(Integer.MAX_VALUE)));
	}

	public static VariableNode createRandomVariableNode() {
		StringBuffer buffer = new StringBuffer();
		synchronized (variableCharIndex) {
			for (int i = 0; i < variableCharIndex.length; ++i) {
				if (variableCharIndex[i] >= variableCharacters.length) {
					if (i < variableCharIndex.length - 1) {
						variableCharIndex[i] = 0;
						++(variableCharIndex[i + 1]);
					} else {
						throw new IllegalArgumentException();
					}
				}
				buffer.append(variableCharacters[variableCharIndex[i]]);
			}
		}
		return _factory.createVariableNode(null, buffer.toString());
	}

	public static LiteralNode createRandomLongLiteralNode() {
		return _factory.createLiteralNode(new Long(new Random().nextInt(Integer.MAX_VALUE)));
	}
}
