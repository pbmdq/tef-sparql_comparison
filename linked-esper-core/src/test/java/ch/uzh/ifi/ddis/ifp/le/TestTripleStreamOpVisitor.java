package ch.uzh.ifi.ddis.ifp.le;

/*
 * #%L
 * Linked Data Esper
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import ch.uzh.ifi.ddis.ifp.algebra.BasicGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.FactOp;
import ch.uzh.ifi.ddis.ifp.algebra.GroupGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.Op;
import ch.uzh.ifi.ddis.ifp.algebra.ProjectionOp;
import ch.uzh.ifi.ddis.ifp.algebra.impl.Utilities;
import ch.uzh.ifi.ddis.ifp.algebra.parser.FunctionalAlgebraLexer;
import ch.uzh.ifi.ddis.ifp.algebra.parser.FunctionalAlgebraParser;
import ch.uzh.ifi.ddis.ifp.le.TripleStreamCommonOpVisitor.QueryAndTypeMap;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.EventType;
import com.espertech.esper.client.UpdateListener;
import com.espertech.esper.client.soda.StreamSelector;
import com.espertech.esper.client.time.CurrentTimeEvent;
import com.espertech.esper.event.NaturalEventBean;
import com.espertech.esper.event.map.MapEventBean;

/**
 * <p>
 * Test for {@link TripleStreamCommonOpVisitor}
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class TestTripleStreamOpVisitor {

	private static final Logger _log = LoggerFactory
			.getLogger(TestTripleStreamOpVisitor.class);

	@Test(dataProvider = "queryprovider", dataProviderClass = ch.uzh.ifi.ddis.ifp.algebra.test.QueryProvider.class)
	@Parameters({ "query", "totalNum" })
	public void test(String query, int currentNum, int totalNum) {
		try {
			InputStream in = new ByteArrayInputStream(query.getBytes());
			CharStream input = new ANTLRInputStream(in);
			FunctionalAlgebraLexer lexer = new FunctionalAlgebraLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			FunctionalAlgebraParser parser = new FunctionalAlgebraParser(tokens);

			Op opResult = (Op) parser.query().expr;
			Configuration esperConfig = new Configuration();
			esperConfig.getEngineDefaults().getThreading()
					.setInternalTimerEnabled(false);
			esperConfig
					.getEngineDefaults()
					.getStreamSelection()
					.setDefaultStreamSelector(
							StreamSelector.RSTREAM_ISTREAM_BOTH);
			String eventTypeName = "Triple";

			String printEventTypeName = "print";
			esperConfig.addEventType(printEventTypeName, TripleBase.class);

			esperConfig.addEventType(eventTypeName, TripleNestedType.class);

			final EPServiceProvider epService = EPServiceProviderManager
					.getDefaultProvider(esperConfig);

			final TripleStreamCommonOpVisitor visitor = new TripleStreamCommonOpVisitor(
					epService.getEPAdministrator(), eventTypeName);
			// epService.getEPAdministrator().

			QueryAndTypeMap result = null;
			if (opResult instanceof BasicGraphPatternOp)
				result = visitor.visit((BasicGraphPatternOp) opResult);
			else if (opResult instanceof GroupGraphPatternOp)
				result = visitor.visit((GroupGraphPatternOp) opResult);
			else if (opResult instanceof ProjectionOp)
				result = visitor.visit((ProjectionOp) opResult);
			else if (opResult instanceof FactOp)
				result = visitor.visit((FactOp) opResult);

			for (EventType et : epService.getEPAdministrator()
					.getConfiguration().getEventTypes()) {
				_log.info("Esper config has event type: '{}'", et.getName());
			}

			// fullTest( epService);
			if (currentNum == totalNum - 1) {
				for (String tmp : epService.getEPAdministrator()
						.getStatementNames()) {
					EPStatement stmt = epService.getEPAdministrator()
							.getStatement(tmp);
					// System.out.println(stmt.getText());
					_log.info("Final statment: '{}'", stmt.getText());
					if (stmt.getText().contains("merge")) {
						// stmt.addListener(new CEPListener());
						stmt.setSubscriber(new MySubscriber());
					}
					if (stmt.getText().contains("< 40")) {
						// stmt.addListener(new CEPListener());
						stmt.setSubscriber(new MySubscriber());
					}
					// stmt.addListener(new CEPListener());
					 stmt.setSubscriber(new MySubscriber());
				}
				//singleTest(epService, 0);
				// fullTest(epService);
				countingVistaTest(epService, 0);
				
				
				String epl = "on print select * from fact_01";
				EPStatement statement = epService.getEPAdministrator()
						.createEPL(epl);
				statement.addListener(new CEPListener());
				statement.setSubscriber(new MySubscriber());
				TripleBase event = new TripleBase(1L, 1L);
				epService.getEPRuntime().sendEvent(event);
				 
				System.out.println("end of test");
			}
		} catch (Exception e) {
			_log.error("Eror during test!", e);
			assert false;
		}
	}

	public void fullTest(EPServiceProvider epService) {
		for (int i = 0; i <= 11; i++) {
			File inputFile;
			if (i < 10)
				inputFile = new File(
						"/Users/shengao/Documents/Research/srBench/charley.0"
								+ i + ".n5");
			else
				inputFile = new File(
						"/Users/shengao/Documents/Research/srBench/charley."
								+ i + ".n5");

			Scanner fileScanner = null;
			try {
				fileScanner = new Scanner(inputFile);
			} catch (Exception e) {
			}
			Long systemTime = 0L;
			while (fileScanner.hasNext()) {
				String inputString = fileScanner.nextLine();
				String[] splitInput = inputString.split(" ");

				TripleNestedType event = new TripleNestedType(splitInput[2],
						splitInput[3], splitInput[4],
						Long.parseLong(splitInput[0]) * 1000,
						Long.parseLong(splitInput[1]) * 1000);
				systemTime = setSystemTime(epService, systemTime,
						Long.parseLong(splitInput[0]));
				epService.getEPRuntime().sendEvent(event);

			}
		}
	}

	public void singleTest(EPServiceProvider epService, int i) {
		File inputFile;
		if (i < 10) {
			inputFile = new File(
					"/Users/shengao/Documents/Research/srBench/charley.0" + i
							+ ".n5");
			inputFile = new File(
					"/Users/shengao/Documents/Research/srBench/testing.txt");
		} else
			inputFile = new File(
					"/Users/shengao/Documents/Research/srBench/charley." + i
							+ ".n5");

		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(inputFile);
		} catch (Exception e) {
		}
		int totalinputNum = 10000000;
		int counter = 0;
		Long systemTime = 0L;
		while (fileScanner.hasNext() && counter <= totalinputNum) {
			String inputString = fileScanner.nextLine();
			String[] splitInput = inputString.split(" ");

			TripleNestedType event = new TripleNestedType(splitInput[2],
					splitInput[3], splitInput[4],
					Long.parseLong(splitInput[0]) * 1000,
					Long.parseLong(splitInput[1]) * 1000);

			counter++;
			systemTime = setSystemTime(epService, systemTime,
					Long.parseLong(splitInput[0]));

			epService.getEPRuntime().sendEvent(event);
		}

	}

	public void countingVistaTest(EPServiceProvider epService, int i) {
		File inputFile = new File(
				"/Users/shengao/Documents/Research_data/VistaTV/Event/log_n_tuple_test.csv");

		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(inputFile);
		} catch (Exception e) {
		}
		int totalinputNum = 10000000;
		int counter = 0;
		Long systemTime = 0L;
		while (fileScanner.hasNext() && counter <= totalinputNum) {
			String inputString = fileScanner.nextLine();
			String[] splitInput = inputString.split(" ");

			TripleNestedType event = new TripleNestedType(splitInput[2],
					splitInput[3], splitInput[4],
					Long.parseLong(splitInput[0]) * 1000,
					Long.parseLong(splitInput[1]) * 1000);

			counter++;
			systemTime = setSystemTime(epService, systemTime,
					Long.parseLong(splitInput[0]));

			epService.getEPRuntime().sendEvent(event);
		}

	}

	public Long setSystemTime(EPServiceProvider epService, Long systemTime,
			Long inputTime) {
		if (inputTime > systemTime) {
			systemTime = inputTime;
			CurrentTimeEvent timeEvent = new CurrentTimeEvent(systemTime);
			epService.getEPRuntime().sendEvent(timeEvent);
		}
		return systemTime;
	}

	public static class CEPListener implements UpdateListener {
		@Override
		public void update(EventBean[] newEvents, EventBean[] oldEvents) {
			if (newEvents != null) {
				// _log.debug("New Event result: " +
				// Utilities.printResultEvent((MapEventBean)newEvents[0]);

				// System.out.println(((HashMap)(newEvents[0].getUnderlying())).size()
				// + "listener");
			}
			if (oldEvents != null) {
				// _log.debug("Old Event result: " +
				// System.out.println(Arrays.toString(((NaturalEventBean)oldEvents[0]).getDecoratingProperties().entrySet().toArray()));
				// System.out.println(oldEvents[0].toString());
				// Utilities.printResultEvent((MapEventBean)oldEvents[0]);

				// System.out.println(((HashMap)(oldEvents[0].getUnderlying())).size()+
				// "listener old");
			}
		}
	}

	public class MySubscriber {
		public void update(Object result) {
			System.out.println("sub0 " + result.toString());
		}

		public void update(Object result, Object result1, long start, long end) {
			System.out.println("sub1 " + result.toString() + " "
					+ result1.toString() + " " + Utilities.getTimeStamp(start)
					+ " " + Utilities.getTimeStamp(end));
		}

		public void update(Object result, Object result1, Object result2,
				Object result3, long start, long end) {
			System.out.println("sub2 " + result.toString() + " "
					+ result3.toString() + " " + Utilities.getTimeStamp(start)
					+ " " + Utilities.getTimeStamp(end));
		}

		public void update(Map result, Object result1) {
			System.out.println("sub3 " + result.toString() + " "
					+ result1.toString());
		}

	}

}