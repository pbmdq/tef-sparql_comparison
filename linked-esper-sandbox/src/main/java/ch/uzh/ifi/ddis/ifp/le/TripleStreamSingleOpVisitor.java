package ch.uzh.ifi.ddis.ifp.le;

/*
 * #%L
 * Linked Data Esper Sandbox
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.ddis.ifp.algebra.BasicGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.FilterOp;
import ch.uzh.ifi.ddis.ifp.algebra.GroupGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.Node;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;
import ch.uzh.ifi.ddis.ifp.algebra.ProjectionOp;
import ch.uzh.ifi.ddis.ifp.algebra.TriplePattern;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;

import com.espertech.esper.client.ConfigurationOperations;
import com.espertech.esper.client.soda.CompareListExpression;
import com.espertech.esper.client.soda.Conjunction;
import com.espertech.esper.client.soda.ConstantExpression;
import com.espertech.esper.client.soda.EPStatementObjectModel;
import com.espertech.esper.client.soda.Expression;
import com.espertech.esper.client.soda.FilterStream;
import com.espertech.esper.client.soda.FromClause;
import com.espertech.esper.client.soda.MaxRowExpression;
import com.espertech.esper.client.soda.MinRowExpression;
import com.espertech.esper.client.soda.PropertyValueExpression;
import com.espertech.esper.client.soda.SelectClause;
import com.espertech.esper.event.map.MapEventType;

/**
 * <p>
 * Visitor pattern for streams of time annotated triple patterns.
 * </p>
 * <p>
 * Note: This class is not yet ready to use!!!
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class TripleStreamSingleOpVisitor implements
		OpVisitorReturn<EPStatementObjectModel> {

	private static final Logger _log = LoggerFactory
			.getLogger(TripleStreamSingleOpVisitor.class);

	private final ConfigurationOperations _esperConfig;

	private final UUID _uuid;

	private String _eventTypeName;

	public static final String SELECT_SUBJECT = "subject";
	public static final String SELECT_PREDICATE = "predicate";
	public static final String SELECT_OBJECT = "object";

	/**
	 * 
	 * @param esperConfig
	 * @param eventTypeName
	 */
	public TripleStreamSingleOpVisitor(ConfigurationOperations esperConfig,
			String eventTypeName) {
		if (esperConfig == null) {
			throw new NullPointerException();
		}
		if (eventTypeName == null) {
			throw new NullPointerException();
		}
		if (esperConfig.getEventType(eventTypeName) == null) {
			throw new IllegalArgumentException();
		}
		_esperConfig = esperConfig;
		_eventTypeName = eventTypeName;
		_uuid = UUID.randomUUID();
	}

	@Override
	public EPStatementObjectModel visit(ProjectionOp projectionOp) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EPStatementObjectModel visit(GroupGraphPatternOp graphPatternOp) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EPStatementObjectModel visit(FilterOp filterOp) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * <p>
	 * For each triple pattern, we add a new {@link MapEventType} to the Esper
	 * {@link ConfigurationOperations}. No optimization takes place at this
	 * stage.
	 * </p>
	 * <p>
	 * Each new {@link MapEventType} gets assigned the following name: <span>
	 * {&quot;uuid&quot;:&quot;subject_var_name&quot;,
	 * &quot;uuid&quot;:&quot;predicate_var_name&quot;,
	 * &quot;uuid&quot;:&quot;object_var_name&quot;}. </span> If any of the
	 * variable names is null, then the corresponding value is &quot;null&quot;.
	 * This JSON naming scheme helps a qurey optimizer later tracing back from
	 * which BGP the {@link MapEventType} originates.
	 * </p>
	 * 
	 * @return
	 */
	@Override
	public EPStatementObjectModel visit(BasicGraphPatternOp basicGraphPatternOp) {
		final EPStatementObjectModel bgpQuery = new EPStatementObjectModel();
		final SelectClause bgpSelectClause = SelectClause.create();
		final FromClause bgpFromClause = FromClause.create();
		final MinRowExpression minRowStartExpr = new MinRowExpression();
		final MaxRowExpression maxRowEndExpr = new MaxRowExpression();

		for (TriplePattern tp : basicGraphPatternOp.getTriplePatterns()) {
			final Node subject = tp.getSubject();
			final Node predicate = tp.getPredicate();
			final Node object = tp.getObject();
			final VariableNode subjectVar = subject.isVariable() ? subject
					.asVariable() : null;
			final VariableNode predicateVar = predicate.isVariable() ? predicate
					.asVariable() : null;
			final VariableNode objectVar = object.isVariable() ? object
					.asVariable() : null;
			final String resultEventTypeName = String.format(
					"{\"%s\":\"%s\",\"%s\":\"%s\",\"%s\":\"%s\"}", _uuid,
					(subjectVar == null ? "null" : subjectVar.toString()),
					_uuid,
					(predicateVar == null ? "null" : predicateVar.toString()),
					_uuid, (objectVar == null ? "null" : objectVar.toString()));

			minRowStartExpr.add(String.format("%s.start", resultEventTypeName));
			maxRowEndExpr.add(String.format("%s.end", resultEventTypeName));

			final Map<String, Object> eventTypeMap = new HashMap<String, Object>();
			if (subjectVar != null) {
				eventTypeMap.put(subjectVar.toString(), String.class);
			}
			if (predicateVar != null) {
				eventTypeMap.put(predicateVar.toString(), String.class);
			}
			if (objectVar != null) {
				eventTypeMap.put(objectVar.toString(), String.class);
			}
			List<Expression> tpFilterChildren = new ArrayList<Expression>();

			// s
			if (subject.isVariable()) {
				bgpSelectClause.addWithAsProvidedName(String.format("%s.%s",
						resultEventTypeName, SELECT_SUBJECT), subjectVar
						.toString());
			} else {
				final CompareListExpression exp = new CompareListExpression(
						true, "=");
				exp.addChild(new PropertyValueExpression(SELECT_SUBJECT));
				exp.addChild(new ConstantExpression(tp.getSubject().toString()));
				tpFilterChildren.add(exp);
			}
			// p
			if (predicate.isVariable()) {
				bgpSelectClause.addWithAsProvidedName(String.format("%s.%s",
						resultEventTypeName, SELECT_PREDICATE),

				predicateVar.toString());
			} else {
				final CompareListExpression exp = new CompareListExpression(
						true, "=");
				exp.addChild(new PropertyValueExpression(SELECT_PREDICATE));
				exp.addChild(new ConstantExpression(tp.getPredicate()
						.toString()));
				tpFilterChildren.add(exp);
			}
			// o
			if (object.isVariable()) {
				bgpSelectClause.addWithAsProvidedName(String.format("%s.%s",
						resultEventTypeName, SELECT_OBJECT), objectVar
						.toString());
			} else {
				final CompareListExpression exp = new CompareListExpression(
						true, "=");
				exp.addChild(new PropertyValueExpression(SELECT_OBJECT));
				exp.addChild(new ConstantExpression(tp.getObject().toString()));
				tpFilterChildren.add(exp);
			}

			Expression tpFilter = new Conjunction();
			tpFilter.setChildren(tpFilterChildren);
			bgpFromClause.add(FilterStream.create(_eventTypeName,
					resultEventTypeName, tpFilter));

		}
		bgpSelectClause.add(minRowStartExpr, "start");
		bgpSelectClause.add(maxRowEndExpr, "end");
		bgpQuery.setFromClause(bgpFromClause);
		bgpQuery.setSelectClause(bgpSelectClause);
		_log.debug("Creating new query: '{}'", bgpQuery.toEPL());

		return bgpQuery;
	}
}
