package ch.uzh.ifi.ddis.ifp.comparison;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class QueriesOfCSingleFact extends Queries{

	public void setQueries (EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		cepAdm.createEPL("create schema countEventType (timeStamp Long , userCount Long, channel String) ");
		EPStatement inertStatement = cepAdm
				.createEPL("     "
						+ "insert into countEventType "
						+ "select  "
						//+ "ce.userCount "
						+ "(select count(*)  "
						+ "from UserEvent.win:time_batch( "+ windowSize +" sec)  as ue "
						+ "where "
						+ "ce.userChannel = ue.userChannel "
						+ "and ue.userAction = '<vistaTV:quit>' "
						//+ "group by userChannel  "
						+ ") "
						+ "+ (select count(*)  "
						+ "from UserEvent.win:time_batch( "+ windowSize +" sec) as ue "
						+ "where "
						+ "ce.userChannel = ue.userChannel "
						+ "and ue.userAction = '<vistaTV:join>' "
						//+ "group by userChannel  "
						+ ") "
						+ " as userCount , ce.userChannel as channel ,  (current_timestamp + 1000L) as timeStamp "
						+ "from "
						+ "UserEvent.win:time_batch( "+ windowSize +" sec) as ce "
						+ "group by ce.userChannel  "
						);
		EPStatement countEventCopyStatement = cepAdm
				.createEPL("insert into countEventType "
						+ "select sum(userCount) as userCount, channel as channel , (current_timestamp + 1000L) as timeStamp "
						+ "from countEventType.win:time_batch( "+ windowSize +" sec) "
						+ "group by channel  "
						//+ "having sum(userCount) > 0 "
						+ " ");
		
		EPStatement initChanneltatement = 
				cepAdm.createEPL("                                                           "
				//+ "context incrementByChannel "
				+ "insert into countEventType "
				+ "select 1L as userCount, ue.userChannel as channel , ue.timeStamp as timeStamp "
				+ "from UserEvent(userAction='<vistaTV:join>').win:length(1) as ue "
				+ "Where not exists  "
				+ "( "
				+ "select *  "
				+ "from countEventType.win:time_batch( "+ windowSize +" sec) as ce where ce.channel = ue.userChannel "
				+ ") "
				);
		
		EPStatement countEventLogStatement = cepAdm
				.createEPL("select sum(userCount) as counter, channel as channel , current_timestamp as currentTime "//, ce.channel "
				+ "from countEventType.win:time_batch( "+ windowSize +" sec) as ce "
				+ "group by ce.channel  "
				+ " ");

		//setStatementSubscriber(inertStatement, "countStatement", isPrinting);
		//setStatementSubscriber(countEventCopyStatement, "countEventCopyStatement", isPrinting);
		
		//setStatementSubscriber(userEventLogStatement, "UserEventLog", isPrinting);
		
		setStatementSubscriber(countEventLogStatement, "countUserResults", isPrinting);
		
		//setStatementSubscriber(testEventCopyStatement, "testEventCopyStatement", isPrinting);
		
		
	}
	public void setQueriesInitByAllChannel (EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		EPStatement countStatement = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch( "+ windowSize +" sec) as ue "
						+ "where "
						+ "ce.userChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch( "+ windowSize +" sec) as ue "
						+ "where "
						+ "ce.userChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ ") "
						+ ", ce.userChannel , (current_timestamp + "+ windowSize*1000 +"L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch( "+ windowSize +" sec) as ce "
						+ "group by ce.userChannel  "
						);
		
		//EPStatement userEventLogStatement = cepAdm
		//		.createEPL("select * from UserEvent");

		EPStatement countEventLogStatement = cepAdm
				.createEPL("select * from CountEventTimeStamp  ");

		//setStatementSubscriber(countStatement, "countStatement", isPrinting);
		//setStatementSubscriber(userEventLogStatement, "UserEventLog", isPrinting);
		setStatementSubscriber(countEventLogStatement, "countEventLog", isPrinting);
		
	}
}
