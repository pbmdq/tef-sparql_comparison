package ch.uzh.ifi.ddis.ifp.comparison;

public class ViewshipEvent {
	Long timeStamp;
	String userName;
	String userAction;
	String EPGProgram;

	public ViewshipEvent(Long timeStamp, String userName, String userAction,
			String EPGProgram) {
		this.timeStamp = timeStamp;
		this.userName = userName;
		this.userAction = userAction;
		this.EPGProgram = EPGProgram;
	}

	public String getEPGProgram() {
		return this.EPGProgram;
	}
	public String getUserAction() {
		return this.userAction;
	}
	public String getUserName() {
		return this.userName;
	}

	public Long getTimeStamp() {
		return this.timeStamp;
	}
	@Override
	public String toString() {
		return "time: " + ConvertTime.ConvertTime(timeStamp) + " User: " + this.userName
				+ " Action: " + this.userAction + " Channel: "
				+ this.EPGProgram;
	}
}