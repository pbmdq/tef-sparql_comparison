package ch.uzh.ifi.ddis.ifp.comparison;

import com.espertech.esper.client.*;
import com.espertech.esper.client.soda.StreamSelector;
import com.espertech.esper.client.time.CurrentTimeEvent;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;

public class comparisonMain {
	public static class CEPListener implements UpdateListener {
		public void update(EventBean[] newData, EventBean[] oldData) {
			/*
			 * if( newData != null) System.out.println("Event received: " +
			 * newData[0].getUnderlying()); if( oldData != null)
			 * System.out.println("Event released: " +
			 * oldData[0].getUnderlying());
			 */
		}
	}
	
	public static Long setSystemTime(EPServiceProvider epService,
			Long systemTime, Long inputTime) {
		if (inputTime > systemTime) {
			systemTime = inputTime;
			CurrentTimeEvent timeEvent = new CurrentTimeEvent(systemTime);
			epService.getEPRuntime().sendEvent(timeEvent);
		}
		return systemTime;
	}

	
	public static void initialEvent(EPServiceProvider epService,
			Long systemTime, Long inputTime) {
		File channelName = new File(
				//"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_half_day_name.csv");
				"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_one_day_name.csv");
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(channelName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (fileScanner.hasNextLine()) {
			String inputString = fileScanner.nextLine();
			CountEventTimeStamp event1 = new CountEventTimeStamp(0L, inputString, inputTime );
			systemTime = setSystemTime(epService, systemTime, inputTime);
 			epService.getEPRuntime().sendEvent(event1);
		}
	}
	public static void experiment(int useCase, int queryLang, int windowSize) {
		Configuration cepConfig = new Configuration();
		cepConfig.getEngineDefaults().getThreading()
				.setInternalTimerEnabled(false);
		cepConfig.getEngineDefaults().getStreamSelection()
				.setDefaultStreamSelector(StreamSelector.RSTREAM_ISTREAM_BOTH);
		cepConfig.addEventType("UserEvent", UserEvent.class.getName());
		// cepConfig.addEventType("CountEvent", CountEvent.class.getName());
		cepConfig.addEventType("CountEventTimeStamp",
				CountEventTimeStamp.class.getName());
		cepConfig.addEventType("CountByChannelEvent",
				CountEventTimeStamp.class.getName());
		cepConfig.addEventType("CountOnlyChannelEvent",
				CountByChannelEvent.class.getName());
		cepConfig.addEventType("CountByProgramEvent",
				CountByProgramEvent.class.getName());
		cepConfig.addEventType("UserEventSession",
				UserEventSession.class.getName());
		
		cepConfig.addEventType("EPGEvent", EPGEvent.class.getName());
		cepConfig.addEventType("ViewshipEvent", ViewshipEvent.class.getName());

		EPServiceProvider cep = EPServiceProviderManager.getProvider(
				"myCEPEngine", cepConfig);
		EPAdministrator cepAdm = cep.getEPAdministrator();
		
		File inputFile = new File(
				//"src/test/resources/testdata.csv");
				"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_session.csv");
		/*
		if(queryLang == 2) {
			inputFile = new File(
					//"src/test/resources/testdata.csv");
					"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_session.csv");
		} else if (queryLang == 3) {
			inputFile = new File(
					//"src/test/resources/testdata.csv");
					"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_session.csv");
		}
		*/
		new File(
				//"src/test/resources/testdata.csv");
				"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_log_session.csv");
				//"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_log.csv");
				//"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_half_day_mix.csv");
				//"/Users/shengao/Documents/Research_data/VistaTV/Event/ISWC_half_day_mix.csv");
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(inputFile);
		} catch (Exception e) {
		}
		int totalInputNum = 100000;
		int counter = 0;
		Long systemTime = 0L;
		systemTime = setSystemTime(cep, systemTime, 1343850000000L);
		
		
		//QueriesOfCSingleFact myQueries = new QueriesOfCSingleFact();
		//myQueries.setQueries(cep, cepAdm, 500, false);
// 10 100 500 for the window size	
		//QueriesOfTEFSingleFact myQueries = new QueriesOfTEFSingleFact();
		//myQueries.setQueries(cep, cepAdm, 500, false);
		
		//QueriesOfEPTwoFacts myQueries = new QueriesOfEPTwoFacts();
		//myQueries.setQueries(cep, cepAdm, 5, true);
		
		//C doesnot work
		//QueriesOfCTwoFacts myQueries = new QueriesOfCTwoFacts();
		//myQueries.setQueries(cep, cepAdm, 5, true);
		
		//QueriesOfTEFTwoFacts myQueries = new QueriesOfTEFTwoFacts();
		//myQueries.setQueries(cep, cepAdm, 500, false);
		boolean needsInit = false;
		Queries myQueries = null;
		if(useCase == 1) {
			if(queryLang == 1) {
				myQueries = new QueriesOfEPSingleFact();
				((QueriesOfEPSingleFact)myQueries).setQueries(cep, cepAdm, windowSize, false);
				needsInit = false;
			}else if(queryLang == 2) {
				myQueries = new QueriesOfCSingleFact();
				((QueriesOfCSingleFact)myQueries).setQueries(cep, cepAdm, windowSize, false);
				needsInit = false;
			} else if (queryLang == 3) {
				myQueries = new QueriesOfTEFSingleFact();
				((QueriesOfTEFSingleFact)myQueries).setQueries(cep, cepAdm, windowSize, false);
			}
		}
		else if(useCase == 2) {
			if(queryLang == 2) {
				myQueries = new QueriesOfCSessionSingleFact();
				((QueriesOfCSessionSingleFact)myQueries).setQueries(cep, cepAdm, windowSize, false);
			} else if (queryLang == 3) {
				myQueries = new QueriesOfTEFSessionSingleFact();
				((QueriesOfTEFSessionSingleFact)myQueries).setQueries(cep, cepAdm, windowSize, false);
			}
		}
		/*
		int i = 0;
		if(i == 0) {
			CountEventTimeStamp event1 = new CountEventTimeStamp(0L, "<channel:a>", 1343858400000L);
			cep.getEPRuntime().sendEvent(event1);
			i++;
		} 
		if (i == 1) {
			CountEventTimeStamp event2 = new CountEventTimeStamp(0L, "<channel:b>", 1343858402000L);
			cep.getEPRuntime().sendEvent(event2);
			i++;
		} 
		if (i == 2) {
			CountEventTimeStamp event3 = new CountEventTimeStamp(0L, "<channel:c>", 1343858403000L);
			cep.getEPRuntime().sendEvent(event3);
			
		}
		*/
		if(needsInit)
			initialEvent(cep, systemTime, 1343858400000L);
		
		long estimatedTime = 0;
		// EPG
		
		/*
		while (fileScanner.hasNext() ) {
			String inputString = fileScanner.nextLine();
			if (inputString.contains("join") || inputString.contains("quit")) {
				continue;
			}
			break;
		}
		*/
		
		while (fileScanner.hasNext() && counter < totalInputNum) {
			String inputString = fileScanner.nextLine();
			long startTime = System.currentTimeMillis();
			String[] splitInput = inputString.split(" ");
			systemTime = setSystemTime(cep, systemTime,
					Long.parseLong(splitInput[0]) * 1000);

			if ( ! splitInput[0].contentEquals(splitInput[1]) ) {
				UserEventSession event = new UserEventSession(
						Long.parseLong(splitInput[0]) * 1000, Long.parseLong(splitInput[1]) ,splitInput[2],
						splitInput[3], splitInput[4]);
				counter++;
				cep.getEPRuntime().sendEvent(event);
			} 
			else if (inputString.contains("join") || inputString.contains("quit")) {
				UserEvent event = new UserEvent(
						Long.parseLong(splitInput[0]) * 1000, splitInput[2],
						splitInput[3], splitInput[4]);
				counter++;
				cep.getEPRuntime().sendEvent(event);
			} else if (inputString.contains("play")
					|| inputString.contains("finish")
					|| inputString.contains("refresh")) {
				EPGEvent event = new EPGEvent(
						Long.parseLong(splitInput[0]) * 1000, splitInput[2],
						splitInput[3], splitInput[4]);
				counter++;
				cep.getEPRuntime().sendEvent(event);
			}
			
			estimatedTime += System.currentTimeMillis() - startTime;
		}
		
		myQueries.printCounters();
		System.out.println("event/sec " + (double)(counter)/((double)(estimatedTime)/(double)1000) +"\n\n");
		cep.destroy();
		// TEF_SPARQL_TEST();
	}

}
