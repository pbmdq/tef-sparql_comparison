package ch.uzh.ifi.ddis.ifp.comparison;
import ch.uzh.ifi.ddis.ifp.comparison.comparisonMain.CEPListener;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class QueriesOfTEFTwoFacts extends Queries{
	public void setQueries(EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		cepAdm.createEPL("create window factUser.win:keepall() as (userName String, channel String, startTime Long, endTime Long) ");
		cepAdm.createEPL("create window factEPG.win:keepall() as (channel String, program String, startTime Long, endTime Long) ");
		cepAdm.createEPL("create window factViewship.win:keepall() as (userName String, program String, startTime Long, endTime Long) ");
		
		cepAdm.createEPL("on UserEvent "
				+ "merge factUser "
				+ "where factUser.channel = UserEvent.userChannel "
				+ "and factUser.userName = UserEvent.userName "
				+ "when matched  and UserEvent.userAction = '<vistaTV:quit>' and endTime = 0L"
				+ "then delete where factUser.userName = UserEvent.userName  "
				+ "when not matched "
				+ "then insert "
				+ "select UserEvent.userName as userName, UserEvent.userChannel as channel, UserEvent.timeStamp as startTime, 0L as endTime");
		
		cepAdm.createEPL("on EPGEvent "
				+ "merge factEPG "
				+ "where factEPG.program = EPGEvent.EPGProgram and factEPG.channel = EPGEvent.EPGChannel "
				+ "when matched and EPGEvent.EPGAction = '<vistaTV:finish>' and endTime = 0L    "
				+ "then delete where factEPG.program = EPGEvent.EPGProgram and factEPG.channel = EPGEvent.EPGChannel "
				+ "when not matched "
				+ "then insert "
				+ "select EPGEvent.EPGChannel as channel, EPGEvent.EPGProgram as program, EPGEvent.timeStamp as startTime, 0L as endTime");
		
		
		cepAdm.createEPL("on UserEvent "
				+ "merge factEPG "
				+ "where UserEvent.userAction = '<vistaTV:join>' And factEPG.channel = UserEvent.userChannel And factEPG.endTime = 0L "
				+ "when matched "
				+ "then insert into factViewship "
				+ "select UserEvent.userName as userName, factEPG.program as program, UserEvent.timeStamp as startTime, 0L as endTime");

		
		cepAdm.createEPL("on EPGEvent "
				+ "merge factUser "
				+ "where EPGEvent.EPGAction = '<vistaTV:play>' And factUser.channel = EPGEvent.EPGChannel And factUser.endTime = 0L "
				+ "when matched "
				+ "then insert into factViewship "
				+ "select factUser.userName as userName, EPGEvent.EPGProgram as program, EPGEvent.timeStamp as startTime, 0L as endTime");

		// end viewship
		cepAdm.createEPL("on EPGEvent "
				+ "merge factViewship "
				+ "where EPGEvent.EPGAction = '<vistaTV:finish>' And EPGEvent.EPGProgram = factViewship.program And factViewship.endTime = 0L"
				+ "when matched " + "then delete "
				+ "where EPGEvent.EPGAction = '<vistaTV:finish>' And EPGEvent.EPGProgram = factViewship.program And factViewship.endTime = 0L ");
		
		/*
		EPStatement selectUserEventStatement = cepAdm
				.createEPL("select * from UserEvent");
		setStatementSubscriber(selectUserEventStatement, "selectAllUserEvent", isPrinting);
		
		EPStatement selectEPGEventStatement = cepAdm
				.createEPL("select * from EPGEvent");
		setStatementSubscriber(selectEPGEventStatement, "selectAllEPGEvent", isPrinting);
		
		EPStatement selectFactUserStatement = cepAdm
				.createEPL(" on factUser select count(*) from factUser ");
		setStatementSubscriber(selectFactUserStatement, "selectAllFactUser", isPrinting);
		
		EPStatement selectFactEPG = cepAdm
				.createEPL(" on factEPG select count(*) from factEPG ");
		setStatementSubscriber(selectFactEPG, "selectAllFactEPG", isPrinting);
		
		EPStatement selectViewshipStatement = cepAdm
				.createEPL(" on factViewship select count(*) from factViewship ");
		setStatementSubscriber(selectViewshipStatement, "selectAllViewship", isPrinting);
		*/
		EPStatement countViewship = cepAdm
				.createEPL("select count(*), program from factViewship group by program output all every "+ windowSize +" sec");
		setStatementSubscriber(countViewship, "countViewshipCount", isPrinting);
		
	}
	public static void setQueryNoDelete(EPServiceProvider cep, EPAdministrator cepAdm) {
		cepAdm.createEPL("create window factUser.win:keepall() as (userName String, channel String, startTime Long, endTime Long) ");
		cepAdm.createEPL("create window factEPG.win:keepall() as (channel String, program String, startTime Long, endTime Long) ");
		cepAdm.createEPL("create window factViewship.win:keepall() as (userName String, program String, startTime Long, endTime Long) ");
		cepAdm.createEPL("on UserEvent "
				+ "merge factUser "
				+ "where factUser.channel = UserEvent.userChannel "
				+ "when matched AND factUser.userName = UserEvent.userName  and UserEvent.userAction = '<vistaTV:quit>' and endTime = 0L"
				+ "then update set factUser.endTime = UserEvent.timeStamp "
				+ "when not matched "
				+ "then insert "
				+ "select UserEvent.userName as userName, UserEvent.userChannel as channel, UserEvent.timeStamp as startTime, 0L as endTime");
		cepAdm.createEPL("on UserEvent "
				+ "merge factEPG "
				+ "where UserEvent.userAction = '<vistaTV:join>' And factEPG.channel = UserEvent.userChannel And factEPG.endTime = 0L "
				+ "when matched "
				+ "then insert into factViewship "
				+ "select UserEvent.userName as userName, factEPG.program as program, UserEvent.timeStamp as startTime, 0L as endTime");

		cepAdm.createEPL("on EPGEvent "
				+ "merge factEPG "
				+ "where factEPG.program = EPGEvent.EPGProgram "
				+ "when matched AND factEPG.channel = EPGEvent.EPGChannel and EPGEvent.EPGAction = '<vistaTV:finish>' and endTime = 0L    "
				+ "then update set factEPG.endTime = EPGEvent.timeStamp "
				+ "when not matched "
				+ "then insert "
				+ "select EPGEvent.EPGChannel as channel, EPGEvent.EPGProgram as program, EPGEvent.timeStamp as startTime, 0L as endTime");

		cepAdm.createEPL("on EPGEvent "
				+ "merge factUser "
				+ "where EPGEvent.EPGAction = '<vistaTV:play>' And factUser.channel = EPGEvent.EPGChannel And factUser.endTime = 0L "
				+ "when matched "
				+ "then insert into factViewship "
				+ "select factUser.userName as userName, EPGEvent.EPGProgram as program, EPGEvent.timeStamp as startTime, 0L as endTime");

		// end viewship
		cepAdm.createEPL("on UserEvent "
				+ "merge factViewship "
				+ "where UserEvent.userAction = '<vistaTV:quit>' And factViewship.userName = UserEvent.userName And factViewship.endTime = 0L"
				+ "when matched " + "then update "
				+ "set factViewship.endTime = UserEvent.timeStamp ");
		cepAdm.createEPL("on EPGEvent "
				+ "merge factViewship "
				+ "where EPGEvent.EPGAction = '<vistaTV:finish>' And EPGEvent.EPGProgram = factViewship.program And factViewship.endTime = 0L"
				+ "when matched " + "then update "
				+ "set factViewship.endTime = EPGEvent.timeStamp ");
		EPStatement selectFactUserStatement = cepAdm
				.createEPL("on factUser select * from factUser");
		selectFactUserStatement.setSubscriber(new MySubscriber("FactUser"));
		EPStatement selectFactEPGStatement = cepAdm
				.createEPL("on factEPG select * from factEPG");
		selectFactEPGStatement.setSubscriber(new MySubscriber("FactEPG"));
		EPStatement selectViewshipStatement = cepAdm
				.createEPL("on factViewship select * from factViewship");
		selectViewshipStatement.setSubscriber(new MySubscriber("FactViewship"));

	}
}
