package ch.uzh.ifi.ddis.ifp.comparison;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertTime {
	public static String ConvertTime(long time) {
		Date date = new Date(time);
		Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		return format.format(date).toString();
	}
}
