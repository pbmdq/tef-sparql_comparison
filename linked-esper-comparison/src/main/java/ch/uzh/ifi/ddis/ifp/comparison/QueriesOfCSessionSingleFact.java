package ch.uzh.ifi.ddis.ifp.comparison;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class QueriesOfCSessionSingleFact extends Queries{
	
public void setQueries (EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
	//EPStatement declareFact = cepAdm
	//		.createEPL("create schema sessionFact as (timeStamp long, originalTime long, session long, userName String, userAction String, userChannel String) ");
 
	EPStatement insertInoNextFact = cepAdm
	 		.createEPL(" "
					+ "insert into UserEventSession "
					+ "select  (current_timestamp + "+windowSize+"000L ) as timeStamp, originalTime as originalTime, ce.session as session, ce.userName as userName, '<vistaTV:join>' as userAction, ce.userChannel as userChannel "
					+ "from UserEventSession.win:time_batch( "+windowSize+" sec ).std:unique(session)  ce "
					+ "Where ce.userAction = '<vistaTV:join>' "
					);
	
		EPStatement outputDuration = cepAdm
				.createEPL(" "
						+ "select  userName, userChannel, (current_timestamp - originalTime), originalTime "
						+ "from UserEventSession.win:time_batch( "+windowSize+" sec ).std:unique(session)"//.win:length(1)"
						+ "where UserEventSession.userAction = '<vistaTV:join>'"
						);
		
		//EPStatement userEventLogStatement = cepAdm
		//		.createEPL("select * from UserEventSession");
		//EPStatement sessionFactStatement = cepAdm
		//		.createEPL("select * from sessionFact");
		
		//setStatementSubscriber(insertIntoFact, "insertIntoFact", isPrinting);
		//setStatementSubscriber(insertQuitIntoFact, "insertQuitIntoFact", isPrinting);
		
		//setStatementSubscriber(userEventLogStatement, "UserEventLog", isPrinting);
		
		//setStatementSubscriber(sessionFactStatement, "SessionFactStream", isPrinting);
		//setStatementSubscriber(outputWhenTerminated, "outputWhenTerminated", isPrinting);
		
		setStatementSubscriber(outputDuration, "outputDuration", isPrinting);
		//setStatementSubscriber(outputJoinDuration, "outputJoinDuration", isPrinting);
		//setStatementSubscriber(outputQuitDuration, "outputQuitDuration", isPrinting);
		//setStatementSubscriber(insertInoNextFact, "insertInoNextFact", isPrinting);
		
	}

public void setQueries_backup (EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
	EPStatement declareFact = cepAdm
			.createEPL("create schema sessionFact as (timeStamp long, session long, userName String, userAction String, userChannel String) ");

	
	EPStatement contextStatement = 
			cepAdm.createEPL("                                                           "
					+ "create context userSessionContext                                  "
					+ "   context userSessionPartition "
					+ "    partition by session "
					+ "    from UserEventSession, "
					+ "   context counting                                           "
					+ "    start UserEventSession(userAction = '<vistaTV:join>') as ce "
					+ "    end  UserEventSession(userAction = '<vistaTV:quit>' and userName = ce.userName and session = ce.session) "
					);
	
	EPStatement insertIntoFact = cepAdm
			.createEPL(" "
					+ "context userSessionContext "
					+ "insert into sessionFact "
					+ "select  current_timestamp as timeStamp, ce.session as session, ce.userName as userName, '<vistaTV:refresh>' as userAction, ce.userChannel as userChannel "
					+ "from UserEventSession.win:length(1) as  ce "
					+ "Where ce.userAction = '<vistaTV:join>' "
					+ "output snapshot every " + windowSize + " sec"
					);
	
	EPStatement outputDuration = cepAdm
			.createEPL(" "
					+ "select   distinct userName "//istream userName, userChannel, (current_timestamp - timeStamp), current_timestamp "
					+ "from sessionFact.std:unique(session).win:length(1)"
					+ "where sessionAction "
					//+ " group by session "
					+ "output snapshot every "+ windowSize +" seconds"
					
					);
	
	EPStatement outputWhenTerminated = cepAdm
			.createEPL(" "
					+ "context userSessionContext "
					+ "select userName, (current_timestamp - timeStamp), userChannel, current_timestamp "
					+ "from UserEventSession(userAction = '<vistaTV:join>') "
					+ "output when terminated"
					);
	
	//EPStatement userEventLogStatement = cepAdm
	//		.createEPL("select * from UserEventSession");
	EPStatement sessionFactStatement = cepAdm
			.createEPL("select * from sessionFact");
	
	
	setStatementSubscriber(insertIntoFact, "insertIntoFact", isPrinting);
	//setStatementSubscriber(userEventLogStatement, "UserEventLog", isPrinting);
	
	//setStatementSubscriber(sessionFactStatement, "SessionFactStream", isPrinting);
	setStatementSubscriber(outputWhenTerminated, "outputWhenTerminated", isPrinting);
	
	setStatementSubscriber(outputDuration, "outputDuration", isPrinting);
	
}



}
