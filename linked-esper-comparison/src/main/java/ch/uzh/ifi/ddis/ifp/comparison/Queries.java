package ch.uzh.ifi.ddis.ifp.comparison;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class Queries {
	Map <String, MySubscriber> subScribers = new HashMap<String, MySubscriber>();
	
	public void setQueries(EPServiceProvider cep, EPAdministrator cepAdm, boolean isPrinting) {
	}
	public void setStatementSubscriber(EPStatement myStatement, String subScribername, boolean isPrinting){
		subScribers.put(subScribername, new MySubscriber(subScribername, isPrinting));
		myStatement.setSubscriber(subScribers.get(subScribername));
	}
	public void printCounters (){
		for( Entry<String, MySubscriber> temp: this.subScribers.entrySet()) {
			System.out.print(temp.getKey() +" "+ temp.getValue().counter +"\n");
		}
	}
}
