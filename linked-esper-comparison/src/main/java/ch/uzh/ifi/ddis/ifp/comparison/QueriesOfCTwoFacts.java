package ch.uzh.ifi.ddis.ifp.comparison;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class QueriesOfCTwoFacts extends Queries{
	public static void setQueries(EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		EPStatement generateRefreshOneStatement = cepAdm
				.createEPL(" "
						+ "insert into EPGEvent "
						+ "select epg.timeStamp + 4000L, epg.EPGProgram , '<vistaTV:refresh>' as EPGAction, epg.EPGChannel "
						+ "from EPGEvent.std:unique(EPGChannel) as epg "
						+ "output snapshot every 4 sec");
		/*
		EPStatement onlyRefresh = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						//+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						//+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ ", epg.EPGChannel , (current_timestamp + 4000L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch(4 sec) as ce,  "
						+ "EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) as epg "
						+ "WHERE ce.userChannel = epg.EPGProgram "
						//+ " "
						+ " and not exists"
						+ "(select * from EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) where EPGAction = '<vistaTV:finish>' or EPGAction = '<vistaTV:play>' )"
						+ "group by epg.EPGChannel "// :time_batch(10
				);
		*/
		EPStatement copyStatement = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select "
						+ "ce.lastof().userCount "
						
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "ce.userChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "ce.userChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ ") "
						
						+ ", ce.userChannel , (current_timestamp + 4000L) "
						+ "from "
						+ "CountEventTimeStamp.std:unique(userChannel) as ce  "
						//+ ", EPGEvent.win:time_batch(4 sec) as epg "
						//+ ", UserEvent.win:time_batch(4 sec) as ueOuter "
						//+ "WHERE ce.userChannel = epg.EPGChannel "
						//+ " and exists "
						//+ " (select EPGProgram from EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) where EPGAction = '<vistaTV:finish>' ) "
						//+ "group by ce.userChannel "
						+ "output snapshot every 4 sec"
						
						//+ "having epg.EPGAction = '<vistaTV:finish>' "// :time_batch(10
				);
		
		EPStatement containsFinishStatement = cepAdm
				.createEPL("     "
						//+ "insert into CountByProgramEvent "
						+ "select "
						//+ "count(*) "
						
						+ "(select userCount from CountEventTimeStamp.std:unique(userChannel) where userChannel = epg.EPGChannel ) "
						
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ "), "
						
						+ " epg.EPGProgram , (current_timestamp) "
						+ "from "
						//+ "CountEventTimeStamp.std:unique(userChannel) as ce  "
						+ " EPGEvent.win:time_batch(4 sec) as epg "
						//+ ", UserEvent.win:time_batch(4 sec) as ueOuter "
						+ "WHERE "
						+ " epg.EPGAction = '<vistaTV:finish>' and "
						//+ "ce.userChannel = epg.EPGChannel "
						+ " exists "
						//+ "( EPGAction = '<vistaTV:finish>' )"
						+ " (select EPGProgram from EPGEvent.win:time_batch(4 sec)  where EPGAction = '<vistaTV:finish>'  ) "
						+ "group by epg.EPGProgram "
						//+ "having epg.EPGAction = '<vistaTV:finish>' "// :time_batch(10
				);
		/*
		EPStatement containsPlayStatement = cepAdm
				.createEPL("     "
						+ "insert into CountByProgramEvent "
						+ "select "
						//+ "count(*) "
						+ "ce.userCount "
						
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ "and ue.timeStamp >= epg.firstof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ "and ue.timeStamp >= epg.firstof().timeStamp "
						+ ") "
						
						+ ", epg.EPGProgram , (current_timestamp) "
						+ "from "
						+ "CountEventTimeStamp.std:unique(userChannel) as ce  "
						+ ", EPGEvent.win:time_batch(4 sec) as epg "
						//+ ", UserEvent.win:time_batch(4 sec) as ueOuter "
						+ "WHERE ce.userChannel = epg.EPGChannel "
						+ " and exists "
						+ " (select EPGProgram from EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) where EPGAction = '<vistaTV:play>' ) "
						+ "group by epg.EPGProgram "
						+ "having epg.EPGAction = '<vistaTV:play>' "// :time_batch(10
				);
	*/
		EPStatement userEventLogStatement = cepAdm
				.createEPL("select * from UserEvent");

		EPStatement EPGEventLogStatement = cepAdm
				.createEPL("select * from EPGEvent");

		EPStatement countEventLogStatement = cepAdm
				.createEPL("select * from CountEventTimeStamp  ");

		EPStatement countEventByPtrogramStatement = cepAdm
				.createEPL("select * from CountByProgramEvent  ");

		
		
		containsFinishStatement.setSubscriber(new MySubscriber(
				"containsFinishStatement"));
		//containsPlayStatement.setSubscriber(new MySubscriber(
		//		"containsPlayStatement"));
		EPGEventLogStatement.setSubscriber(new MySubscriber("EPGEventLog"));
		userEventLogStatement.setSubscriber(new MySubscriber("UserEventLog"));
		countEventLogStatement.setSubscriber(new MySubscriber(
				"CountEventTimeStamp"));
		countEventByPtrogramStatement.setSubscriber(new MySubscriber(
				"countEventByPtrogramStatement"));

	}
	public static void setQueriesGroupByProgram(EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		EPStatement generateRefreshOneStatement = cepAdm
				.createEPL(" "
						+ "insert into EPGEvent "
						+ "select epg.timeStamp + 4000L, epg.EPGProgram , '<vistaTV:refresh>' as EPGAction, epg.EPGChannel "
						+ "from EPGEvent.std:unique(EPGChannel) as epg "
						+ "output snapshot every 4 sec");
		
		EPStatement noRefresh = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						//+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						//+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ ", epg.EPGProgram , (current_timestamp + 4000L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch(4 sec) as ce,  "
						+ "EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) as epg "
						+ "WHERE ce.userChannel = epg.EPGProgram "
						//+ " "
						+ " and not exists"
						+ "(select * from EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) where EPGAction = '<vistaTV:finish>' or EPGAction = '<vistaTV:play>' )"
						+ "group by epg.EPGProgram "// :time_batch(10
				);
		
		EPStatement containsFinishStatement = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ ", epg.EPGProgram , (current_timestamp + 4000L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch(4 sec) as ce,  "
						+ "EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) as epg "
						//+ ", UserEvent.win:time_batch(4 sec) as ueOuter "
						+ "WHERE ce.userChannel = epg.EPGProgram "
						//+ "and ueOuter.EPGProgram =  epg.EPGChannel "
						//+ "and ueOuter.timeStamp < epg.lastof().timeStamp "
						//+ "and ueOuter.userChannel =  epg.EPGChannel "
						+ " and exists "
						+ " (select EPGProgram from EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) where EPGAction = '<vistaTV:finish>' ) "
						+ "group by epg.EPGProgram "
						+ "having epg.EPGAction = '<vistaTV:finish>' "// :time_batch(10
				);
		EPStatement containsPlayStatement = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ "and ue.timeStamp >= epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch(4 sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ "and ue.timeStamp >= epg.lastof().timeStamp "
						+ ") "
						+ ", epg.EPGProgram , (current_timestamp + 4000L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch(4 sec) as ce,  "
						+ "EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) as epg "
						//+ ", UserEvent.win:time_batch(4 sec) as ueOuter "
						+ "WHERE ce.userChannel = epg.EPGProgram "
						//+ "and ueOuter.EPGProgram =  epg.EPGChannel "
						//+ "and ueOuter.timeStamp < epg.lastof().timeStamp "
						//+ "and ueOuter.userChannel =  epg.EPGChannel "
						+ " and exists "
						+ " (select EPGProgram from EPGEvent.std:groupwin(EPGChannel).win:time_batch(4 sec) where EPGAction = '<vistaTV:play>' ) "
						+ "group by epg.EPGProgram "
						+ "having epg.EPGAction = '<vistaTV:play>' "// :time_batch(10
				);
	
		EPStatement userEventLogStatement = cepAdm
				.createEPL("select * from UserEvent");

		EPStatement EPGEventLogStatement = cepAdm
				.createEPL("select * from EPGEvent");

		EPStatement countEventLogStatement = cepAdm
				.createEPL("select * from CountEventTimeStamp  ");

		containsFinishStatement.setSubscriber(new MySubscriber(
				"containsFinishStatement"));
		containsPlayStatement.setSubscriber(new MySubscriber(
				"containsPlayStatement"));
		EPGEventLogStatement.setSubscriber(new MySubscriber("EPGEventLog"));
		userEventLogStatement.setSubscriber(new MySubscriber("UserEventLog"));
		countEventLogStatement.setSubscriber(new MySubscriber(
				"CountEventTimeStamp"));

	}
	public void setQueries_New(EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		windowSize = 4;
		EPStatement generateRefreshOneStatement = cepAdm
				.createEPL(" "
						+ "insert into EPGEvent "
						+ "select epg.timeStamp + "+windowSize*1000+"L, epg.EPGProgram , '<vistaTV:refresh>' as EPGAction, epg.EPGChannel "
						+ "from EPGEvent.std:unique(EPGChannel) as epg "
						+ "output snapshot every "+windowSize+" sec");
		
		EPStatement noRefresh = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch( "+windowSize+" sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						//+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch( "+windowSize+" sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						//+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ ", epg.EPGProgram , (current_timestamp + "+windowSize*1000+"L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch( "+windowSize+" sec) as ce,  "
						+ "EPGEvent.std:groupwin(EPGChannel).win:time_batch( "+windowSize+" sec) as epg "
						+ "WHERE ce.userChannel = epg.EPGProgram "
						//+ " "
						+ " and not exists"
						+ "(select * from EPGEvent.std:groupwin(EPGChannel).win:time_batch( "+windowSize+" sec) where EPGAction = '<vistaTV:finish>' or EPGAction = '<vistaTV:play>' )"
						+ "group by epg.EPGProgram "// :time_batch(10
				);
		
		EPStatement containsFinishStatement = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch( "+windowSize+" sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch("+windowSize+" sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ "and ue.timeStamp < epg.lastof().timeStamp "
						+ ") "
						+ ", epg.EPGProgram , (current_timestamp + "+windowSize*1000+"L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch( "+windowSize+" sec) as ce,  "
						+ "EPGEvent.std:groupwin(EPGChannel).win:time_batch( "+windowSize+" sec) as epg "
						//+ ", UserEvent.win:time_batch(4 sec) as ueOuter "
						+ "WHERE ce.userChannel = epg.EPGProgram "
						//+ "and ueOuter.EPGProgram =  epg.EPGChannel "
						//+ "and ueOuter.timeStamp < epg.lastof().timeStamp "
						//+ "and ueOuter.userChannel =  epg.EPGChannel "
						+ " and exists "
						+ " (select EPGProgram from EPGEvent.std:groupwin(EPGChannel).win:time_batch("+windowSize+" sec) where EPGAction = '<vistaTV:finish>' ) "
						+ "group by epg.EPGProgram "
						+ "having epg.EPGAction = '<vistaTV:finish>' "// :time_batch(10
				);
		EPStatement containsPlayStatement = cepAdm
				.createEPL("     "
						+ "insert into CountEventTimeStamp "
						+ "select  "
						+ "ce.userCount "
						+ "-  (select count(userAction)  "
						+ "from UserEvent.win:time_batch("+windowSize+" sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:quit>' "
						+ "and ue.timeStamp >= epg.lastof().timeStamp "
						+ ") "
						+ "+ (select count(userAction)  "
						+ "from UserEvent.win:time_batch("+windowSize+" sec) as ue "
						+ "where "
						+ "epg.EPGChannel = ue.userChannel "
						+ "and userAction = '<vistaTV:join>' "
						+ "and ue.timeStamp >= epg.lastof().timeStamp "
						+ ") "
						+ ", epg.EPGProgram , (current_timestamp + "+windowSize*1000+"L) "
						+ "from "
						+ "CountEventTimeStamp.std:groupwin(userChannel).win:time_batch("+windowSize+" sec) as ce,  "
						+ "EPGEvent.std:groupwin(EPGChannel).win:time_batch("+windowSize+" sec) as epg "
						//+ ", UserEvent.win:time_batch(4 sec) as ueOuter "
						+ "WHERE ce.userChannel = epg.EPGProgram "
						//+ "and ueOuter.EPGProgram =  epg.EPGChannel "
						//+ "and ueOuter.timeStamp < epg.lastof().timeStamp "
						//+ "and ueOuter.userChannel =  epg.EPGChannel "
						+ " and exists "
						+ " (select EPGProgram from EPGEvent.std:groupwin(EPGChannel).win:time_batch("+windowSize+" sec) where EPGAction = '<vistaTV:play>' ) "
						+ "group by epg.EPGProgram "
						+ "having epg.EPGAction = '<vistaTV:play>' "// :time_batch(10
				);
	
		EPStatement userEventLogStatement = cepAdm
				.createEPL("select * from UserEvent");

		EPStatement EPGEventLogStatement = cepAdm
				.createEPL("select * from EPGEvent");

		EPStatement countEventLogStatement = cepAdm
				.createEPL("select * from CountEventTimeStamp  ");

		
		setStatementSubscriber(generateRefreshOneStatement, "generateRefreshNum", isPrinting);
		
		
		setStatementSubscriber(containsFinishStatement, "containsFinishStatement", isPrinting);
		setStatementSubscriber(containsPlayStatement, "containsPlayStatement", isPrinting);
		setStatementSubscriber(noRefresh, "noRefreshCount", isPrinting);
		
				
		setStatementSubscriber(userEventLogStatement, "userEventLogStatement", isPrinting);
		setStatementSubscriber(EPGEventLogStatement, "EPGEventLogStatement", isPrinting);
		setStatementSubscriber(countEventLogStatement, "countEventLogStatement", isPrinting);
		
	}

}