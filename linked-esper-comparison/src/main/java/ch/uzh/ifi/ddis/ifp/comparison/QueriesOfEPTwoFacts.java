package ch.uzh.ifi.ddis.ifp.comparison;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class QueriesOfEPTwoFacts extends Queries{
	public void setQueries(EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		
		EPStatement generateRefreshOneStatement = cepAdm
				.createEPL(" "
						+ "insert into EPGEvent "
						+ "select epg.timeStamp + "
						+ windowSize*1000+"L"
						+ " , epg.EPGProgram , '<vistaTV:refresh>', epg.EPGChannel "
						+ "from EPGEvent.std:unique(EPGChannel) as epg "
						+ "output snapshot every "+ windowSize +" sec");
	
		EPStatement contextStatement = cepAdm
				.createEPL("                                                           "
						+ "create context countByChannelContext                                  "
						+ "   context CountEventByChannel "
						+ "    partition by userChannel "
						+ "    from CountByChannelEvent, "
						+ "   context counting                                          "
						+ "    initiated by CountByChannelEvent as ce					"
						+ "    terminated by UserEvent(userChannel = ce.userChannel) 	");

		EPStatement countByProgramContextStatement = cepAdm
				.createEPL("                                                           "
						+ "create context countByProgramContext                                  "
						+ "   context CountEventByChannel "
						+ "    partition by userChannel "
						+ "    from CountByChannelEvent, "
						+ "   context counting                                           "
						+ "    initiated by CountByChannelEvent as ce "
						+ "    terminated by EPGEvent(EPGChannel = ce.userChannel ) ");
		EPStatement countByProgramJoinStatement = cepAdm
				.createEPL("                                                           "
						+ "context countByProgramContext "
						+ "insert into CountByProgramEvent "
						+ "select (ce.userCount) as userCount, ee.EPGProgram as userChannel "
						+ "from CountByChannelEvent.win:keepall() as ce, EPGEvent(EPGAction='<vistaTV:refresh>').win:keepall() as ee  where ce.userChannel = ee.EPGChannel output last when terminated ");

		EPStatement incrementByOneStatement = cepAdm
				.createEPL("                                                           "
						+ "context countByChannelContext "
						+ "insert into CountByChannelEvent "
						+ "select (ce.userCount+1) as userCount, ce.userChannel as userChannel , ue.timeStamp as timeStamp "
						+ "from CountByChannelEvent.win:keepall() as ce, UserEvent(userAction='<vistaTV:join>').win:keepall() as ue  where ce.userChannel = ue.userChannel output last when terminated ");

		EPStatement decrementByOneStatement = cepAdm
				.createEPL("                                                           "
						+ "context countByChannelContext "
						+ "insert into CountByChannelEvent "
						+ "select (ce.userCount-1) as userCount, ce.userChannel as userChannel , ue.timeStamp as timeStamp "
						+ "from CountByChannelEvent.win:keepall() as ce, UserEvent(userAction='<vistaTV:quit>').win:keepall() as ue  where ce.userChannel = ue.userChannel output last when terminated ");

		/*
		EPStatement userEventLogStatement = cepAdm
				.createEPL("select * from UserEvent");
		EPStatement countEventLogStatement = cepAdm
				.createEPL("select * from CountByChannelEvent");
		EPStatement EPGEventLogStatement = cepAdm
				.createEPL("select * from EPGEvent");
		*/
		EPStatement CountByProgramEventLogStatement = cepAdm
				.createEPL("select * from CountByProgramEvent");

		//setStatementSubscriber(incrementByOneStatement, "incrementByOneStatement", isPrinting);
		//setStatementSubscriber(decrementByOneStatement, "decrementByOneStatement", isPrinting);
		//setStatementSubscriber(countByProgramJoinStatement, "decrementByOneStatement", isPrinting);
		
				
		//setStatementSubscriber(userEventLogStatement, "userEventLogStatement", isPrinting);
		//setStatementSubscriber(EPGEventLogStatement, "EPGEventLogStatement", isPrinting);
		//setStatementSubscriber(countEventLogStatement, "countEventLogStatement", isPrinting);
		setStatementSubscriber(CountByProgramEventLogStatement, "CountByProgramEventLogStatement", isPrinting);
		//setStatementSubscriber(generateRefreshOneStatement, "generateRefreshOneStatement", isPrinting);
		
		//generateRefreshOneStatement
	}

	public static void setQueriesTest(EPServiceProvider cep,
			EPAdministrator cepAdm) {
		cepAdm.createEPL("                                                           "
				+ "create context EPGScopeContext1 "
				+ "  context EPGScopeContextByChannel "
				+ "	   partition by EPGChannel from EPGEvent "
				+ ", context EPGAsOuterScope                                           "
				+ "    initiated by EPGEvent(EPGAction = '<vistaTV:play>') as  EPGSPlay "
				+ "    terminated by UserEvent( userChannel = EPGSPlay.EPGChannel) ");

		cepAdm.createEPL("                                                           "
				+ "create context EPGScopeContext2 "
				+ "  context EPGScopeContextByChannel "
				+ "	   partition userChannel from UserEvent "
				+ ", context EPGAsOuterScope                                           "
				+ "    initiated by UserEvent(userAction = '<vistaTV:join>') as  ue "
				+ "    terminated by EPGEvent( ue.userChannel = EPGChannel) ");
		EPStatement EPGContextUserWatchStatement1 = cepAdm
				.createEPL("     "
						+ "context EPGScopeContext1  "
						+ "insert into ViewshipEvent "
						+ "select ue.timeStamp as timeStamp, ue.userName as userName, '<vistaTV:watch>' as userAction, epg.EPGProgram as EPGProgram "
						+ "from  "// :length_batch(3)
						+ "  UserEvent.win:keepall() as ue"
						+ ", EPGEvent.win:keepall() as epg  "
						+ "WHERE ue.userAction = '<vistaTV:join>' "
				// + " OR   EPGAction = '<user:watch>'"
				);

		EPStatement EPGContextUserWatchStatement2 = cepAdm
				.createEPL("     "
						+ "context EPGScopeContext2  "
						+ "insert into ViewshipEvent "
						+ "select ue.timeStamp as timeStamp, ue.userName as userName, '<vistaTV:watch>' as userAction, epg.EPGProgram as EPGProgram "
						+ "from  "// :length_batch(3)
						+ "  UserEvent.win:keepall() as ue"
						+ ", EPGEvent.win:keepall() as epg  "
						+ "WHERE epg.EPGAction = '<vistaTV:play>' "
				// + " OR   EPGAction = '<user:watch>'"
				);

		cepAdm.createEPL("                                                           "
				+ "create context ViewScopeContext1 "
				+ "  context EPGScopeContextByChannel "
				+ "	   partition by userName from ViewshipEvent , userName from UserEvent"
				+ ", context EPGAsOuterScope                                           "
				+ "    initiated by ViewshipEvent(userAction = '<vistaTV:watch>') as  ViewShip "
				+ "    terminated by pattern [EPGEvent( ViewShip.EPGProgram = EPGProgram) or UserEvent( userName = ViewShip.userName)]"
		// + "    terminated by  EPGEvent( ViewShip.EPGProgram = EPGProgram) "

		);

		cepAdm.createEPL("                                                           "
				+ "create context ViewScopeContext1 "
				+ "  context EPGScopeContextByChannel "
				+ "	   partition by userName from ViewshipEvent , userName from UserEvent"
				+ ", context EPGAsOuterScope                                           "
				+ "    initiated by ViewshipEvent(userAction = '<vistaTV:endWatch>') as  ViewShip "
				+ "    terminated by pattern [EPGEvent( ViewShip.EPGProgram = EPGProgram) or UserEvent( userName = ViewShip.userName)]"
		// + "    terminated by  EPGEvent( ViewShip.EPGProgram = EPGProgram) "

		);

		EPStatement ViewScopeContextStatement1 = cepAdm
				.createEPL("     "
						+ "context ViewScopeContext1  "
						+ "insert into ViewshipEvent "
						+ "select epg.timeStamp as timeStamp, ue.userName as userName, '<vistaTV:endWatch>' as userAction, epg.EPGProgram as EPGProgram "
						+ "from  "// :length_batch(3)
						+ "  ViewshipEvent.win:keepall() as ue"
						+ ", EPGEvent.win:keepall() as epg  "
						// + ", UserEvent.win:keepall() as ue  "
						+ " WHERE epg.EPGAction = '<vistaTV:finish>' "
				// +
				// "WHERE ue.userAction = '<vistaTV:quit>' or epg.EPGAction = '<vistaTV:finish>' "
				// + " OR   EPGAction = '<user:watch>'"
				);

		/*
		 * EPStatement EPGContextUserWatchStatement2 = cepAdm.createEPL("     "
		 * + "context EPGScopeContext2  " + "insert into ViewshipEvent " +
		 * "select ue.timeStamp as timeStamp, ue.userName as userName, '<user:watch>' as userAction, epg.EPGProgram as EPGProgram "
		 * + "from  "//:length_batch(3) + "  UserEvent.win:keepall() as ue" +
		 * ", EPGEvent.win:keepall() as epg  " +
		 * "WHERE epg.EPGAction = '<vistaTV:play>' " //+
		 * " OR   EPGAction = '<user:watch>'" );
		 */

		/*
		 * EPStatement contextStatement = cepAdm.createEPL(
		 * "                                                           " +
		 * "create context countByProgram                                  " +
		 * "   context CountEventByProgram " + "    partition by userChannel " +
		 * "    from CountEvent, " +
		 * "   context viewshipEventContext                                           "
		 * + "    initiated by CountEvent as ce " +
		 * "    terminated by ViewshipEvent(EPGProgram = ce.userChannel) " );
		 * 
		 * 
		 * EPStatement incrementByOneStatement = cepAdm.createEPL(
		 * "                                                           " +
		 * "context countByProgram " + "insert into CountEvent " +
		 * "select (ce.userCount+1) as userCount, ue.EPGProgram as userChannel "
		 * +
		 * "from CountEvent.win:keepall() as ce, ViewshipEvent(userAction='<user:watch>').win:keepall() as ue  where ce.userChannel = ue.EPGProgram output last when terminated "
		 * );
		 * 
		 * EPStatement decrementByOneStatement = cepAdm.createEPL(
		 * "                                                           " +
		 * "context countByProgram " + "insert into CountEvent " +
		 * "select (ce.userCount-1) as userCount, ue.EPGProgram as userChannel "
		 * +
		 * "from CountEvent.win:keepall() as ce, ViewshipEvent(userAction='<user:endWatch>').win:keepall() as ue  where ce.userChannel = ue.EPGProgram output last when terminated "
		 * );
		 */

		EPStatement userEventLogStatement = cepAdm
				.createEPL("select * from UserEvent");

		EPStatement countEventLogStatement = cepAdm
				.createEPL("select * from CountEvent");

		EPStatement EPGEventLogStatement = cepAdm
				.createEPL("select * from EPGEvent");

		EPStatement ViewshipEventLogStatement = cepAdm
				.createEPL("select * from ViewshipEvent");

		// selectIntoStatement.setSubscriber(new MySubscriber("selectInto"));

		// EPGContextUserWatchStatement.setSubscriber(new
		// MySubscriber("EPGContextUserWatchStatement"));
		// EPGContextUserEndWatchStatement.setSubscriber(new
		// MySubscriber("EPGContextUserEndWatchStatement"));

		// UserContextUserWatchStatement.setSubscriber(new
		// MySubscriber("UserContextUserWatchStatement"));
		// UserContextUserEndWatchStatement.setSubscriber(new
		// MySubscriber("UserContextUserWatchStatement"));
		EPGContextUserWatchStatement1.setSubscriber(new MySubscriber(
				"EPGContextUserWatchStatement1"));

		userEventLogStatement.setSubscriber(new MySubscriber("UserEventLog"));
		// countEventLogStatement.setSubscriber(new
		// MySubscriber("CountEventLog"));
		EPGEventLogStatement.setSubscriber(new MySubscriber("EPGEventLog"));

		ViewshipEventLogStatement.setSubscriber(new MySubscriber(
				"ViewshipEventLog"));

		// incrementByOneStatement.setSubscriber(new
		// MySubscriber("incrementByOneLog"));
		// decrementByOneStatement.setSubscriber(new
		// MySubscriber("decrementByOneLog"));
/*
		int i = 0;
		if (i == 0) {
			CountEvent event1 = new CountEvent(0L, "<program:1>");
			cep.getEPRuntime().sendEvent(event1);
			i++;
		}
		if (i == 1) {
			CountEvent event2 = new CountEvent(0L, "<program:2>");
			cep.getEPRuntime().sendEvent(event2);
			i++;
		}
		if (i == 2) {
			CountEvent event3 = new CountEvent(0L, "<program:3>");
			cep.getEPRuntime().sendEvent(event3);

		}
		*/
	}
	

}
