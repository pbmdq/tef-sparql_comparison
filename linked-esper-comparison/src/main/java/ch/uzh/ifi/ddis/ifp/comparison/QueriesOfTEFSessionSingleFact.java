package ch.uzh.ifi.ddis.ifp.comparison;

import ch.uzh.ifi.ddis.ifp.comparison.comparisonMain.CEPListener;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class QueriesOfTEFSessionSingleFact extends Queries {
	public void setQueries(EPServiceProvider cep, EPAdministrator cepAdm,
			int windowSize, boolean isPrinting) {
		cepAdm.createEPL("create window factUser.win:keepall() as (userName String, channel String, startTime Long, endTime Long) ");
		EPStatement userFactStatment = cepAdm.createEPL("on UserEvent "
				+ "merge factUser "
				+ "where factUser.channel = UserEvent.userChannel "
				+ "and factUser.userName = UserEvent.userName "
				+ "when matched  and UserEvent.userAction = '<vistaTV:quit>' and endTime = 0L"
				+ "then delete where factUser.userName = UserEvent.userName  "
				+ "when not matched "
				+ "then insert "
				+ "select UserEvent.userName as userName, UserEvent.userChannel as channel, UserEvent.timeStamp as startTime, 0L as endTime");
		
		EPStatement outputDuration = cepAdm
				.createEPL("select current_timestamp, * from factUser output snapshot every "+ windowSize +" sec");
		
		//EPStatement userEventLogStatement = cepAdm
		//	.createEPL("select * from UserEvent");

		//setStatementSubscriber(userEventLogStatement, "UserEventLog", isPrinting);
		//setStatementSubscriber(userFactStatment, "userFactStatment", isPrinting);
		//setStatementSubscriber(userFactLog, "userFactLog", isPrinting);
	
		setStatementSubscriber(outputDuration, "outputDuration", isPrinting);

	}
	public static void setQueryCompilerStyle(EPServiceProvider cep, EPAdministrator cepAdm,
			boolean isPrinting) {
		cepAdm.createEPL("create window UserFact.win:keepall() as (channel String, userCount Integer) ");
		EPStatement sineStatment = cepAdm
				.createEPL("insert into cache_01 select irstream tp_0.userChannel as channel from UserEvent(userAction = any ('<vistaTV:join>')).win:keepall() as tp_0");
		EPStatement untilStatment = cepAdm
				.createEPL("insert into cache_02 select irstream tp_1.userChannel as channel from UserEvent(userAction = any ('<vistaTV:quit>')).win:keepall() as tp_1");

		cepAdm.createEPL("on userEvent " + "merge UserFact "
				+ "where fact_01.channel = cache_01.channel " + "when matched "
				+ "then update set fact_01.userCount = fact_01.userCount + 1"
				+ "when not matched " + "then insert "
				+ "select cache_01.channel as channel, 1 as userCount");

		cepAdm.createEPL("on cache_02 " + "merge fact_01 "
				+ "where fact_01.channel = cache_02.channel " + "when matched "
				+ "then update set fact_01.userCount = fact_01.userCount - 1");

		EPStatement cepStatement = cepAdm
				.createEPL("on fact_01 select * from fact_01");

		EPStatement userEventLogStatement = cepAdm
				.createEPL("select * from UserEvent");

		EPStatement countEventLogStatement = cepAdm
				.createEPL("select * from CountEventTimeStamp  ");
/*
		setStatementSubscriber(countStatement, "countStatement", isPrinting);
		setStatementSubscriber(userEventLogStatement, "UserEventLog",
				isPrinting);
		setStatementSubscriber(countEventLogStatement, "CountEventTimeStamp",
				isPrinting);
*/
	}
}
