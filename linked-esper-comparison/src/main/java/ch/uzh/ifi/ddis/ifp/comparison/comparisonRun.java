package ch.uzh.ifi.ddis.ifp.comparison;

public class comparisonRun {
	public static void main(String[] args) {
		int useCase = 1;
		int queryType = 3;
		
		comparisonMain.experiment(useCase, queryType, 10);
		comparisonMain.experiment(useCase, queryType, 100);
		comparisonMain.experiment(useCase, queryType, 200);
		comparisonMain.experiment(useCase, queryType, 400);
	}	
}
