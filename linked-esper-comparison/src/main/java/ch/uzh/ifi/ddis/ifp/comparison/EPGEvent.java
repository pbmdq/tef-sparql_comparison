package ch.uzh.ifi.ddis.ifp.comparison;

public class EPGEvent {
	Long timeStamp;
	String EPGProgram;
	String EPGAction;
	String EPGChannel;

	public EPGEvent(Long timeStamp, String EPGProgram, String EPGAction,
			String EPGChannel) {
		this.timeStamp 	= timeStamp;
		this.EPGProgram = EPGProgram;
		this.EPGAction 	= EPGAction;
		this.EPGChannel = EPGChannel;
	}

	public Long getTimeStamp() {
		return this.timeStamp;
	}
	public String getEPGAction() {
		return this.EPGAction;
	}
	public String getEPGChannel() {
		return this.EPGChannel;
	}
	public String getEPGProgram() {
		return this.EPGProgram;
	}

	@Override
	public String toString() {
		return "time: " + ConvertTime.ConvertTime(timeStamp) + " User: " + this.EPGProgram
				+ " Action: " + this.EPGAction + " Channel: "
				+ this.EPGChannel;
	}
}