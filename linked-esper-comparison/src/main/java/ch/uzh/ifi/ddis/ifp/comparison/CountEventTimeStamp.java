package ch.uzh.ifi.ddis.ifp.comparison;

public class CountEventTimeStamp {
	Long timeStamp;
	Long userCount;
	String userChannel;

	public CountEventTimeStamp(Long userCount, String userChannel, Long timeStamp) {
		this.userChannel = userChannel;
		this.userCount = userCount;
		this.timeStamp = timeStamp;
	}
	public CountEventTimeStamp( String userChannel, Long userCount, Long timeStamp) {
		this.userChannel = userChannel;
		this.userCount = userCount;
		this.timeStamp = timeStamp;
	}
	public String getUserChannel() {
		return this.userChannel;
	}
	public Long getUserCount() {
		return this.userCount;
	}
	public Long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public String toString() {
		return "time: "
				+ ConvertTime.ConvertTime(timeStamp) +" count:" + this.userCount + " Channel: "
				+ this.userChannel;
	}
}