package ch.uzh.ifi.ddis.ifp.comparison;

import javax.swing.text.Utilities;

public class UserEventSession extends UserEvent{
	Long session;
	Long originalTime;
	
	public UserEventSession(Long timeStamp, Long session, String userName, String userAction,
			String UserChannel ) {
		super(timeStamp, userName, userAction, UserChannel);
		this.session = session;
		//this.originalTime =this.timeStamp;
	}
	public UserEventSession(Long timeStamp, Long original , Long session, String userName, String userAction,
			String UserChannel ) {
		super(timeStamp, userName, userAction, UserChannel);
		//this.originalTime =original;
		this.session = session;
	}

	
	public Long getSession() {
		return this.session;
	}
	public Long getOriginalTime() {
		return this.originalTime;
	}
	
	public Long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public String toString() {

		return "time: " + ConvertTime.ConvertTime(timeStamp) 
				//+ "Original time: " + ConvertTime.ConvertTime(originalTime)
				+ " Session: " + this.session 
				+ " User: "  + this.userName
				+ " Action: " + this.userAction 
				+ " Channel: " + this.userChannel;
	}
}