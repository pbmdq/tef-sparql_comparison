package ch.uzh.ifi.ddis.ifp.comparison;

import javax.swing.text.Utilities;

public class UserEvent {
	Long timeStamp;
	String userName;
	String userAction;
	String userChannel;

	public UserEvent(Long timeStamp, String userName, String userAction,
			String UserChannel) {
		this.timeStamp = timeStamp;
		this.userName = userName;
		this.userAction = userAction;
		this.userChannel = UserChannel;
	}

	public Long getTimeStamp() {
		return this.timeStamp;
	}
	
	public String getUserChannel() {
		return this.userChannel;
	}
	public String getUserAction() {
		return this.userAction;
	}
	public String getUserName() {
		return this.userName;
	}

	@Override
	public String toString() {

		return "time: " + ConvertTime.ConvertTime(timeStamp) + " User: " + this.userName
				+ " Action: " + this.userAction + " Channel: "
				+ this.userChannel;
	}
}