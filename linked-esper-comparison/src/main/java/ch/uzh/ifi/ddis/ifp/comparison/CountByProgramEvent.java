package ch.uzh.ifi.ddis.ifp.comparison;

public class CountByProgramEvent {
	Long timeStamp;
	
	Long userCount;
	String program;

	public CountByProgramEvent(Long userCount, String userChannel, Long timeStamp) {
		this.program = userChannel;
		this.userCount = userCount;
		this.timeStamp = timeStamp;
	}
	public CountByProgramEvent( String userChannel, Long userCount, Long timeStamp) {
		this.program = userChannel;
		this.userCount = userCount;
		this.timeStamp = timeStamp;
	}
	public String getProgram() {
		return this.program;
	}
	public Long getUserCount() {
		return this.userCount;
	}
	public Long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public String toString() {
		return "time: "
				+ ConvertTime.ConvertTime(timeStamp) + "count:" + this.userCount + " Channel: "
				+ this.program;
	}
}