package ch.uzh.ifi.ddis.ifp.comparison;


import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

public class QueriesOfEPSingleFact extends Queries{
	
	public void setQueries(EPServiceProvider cep, EPAdministrator cepAdm, int windowSize, boolean isPrinting) {
		
		EPStatement contextStatement = 
				cepAdm.createEPL("                                                           "
						+ "create context incrementByChannel                                  "
						+ "   context CountEventByChannel "
						+ "    partition by userChannel "
						+ "    from CountByChannelEvent, "
						+ "   context counting                                           "
						+ "    initiated by CountByChannelEvent as ce "
						+ "    terminated by UserEvent(userChannel = ce.userChannel) "
						);
		EPStatement incrementByOneStatement = 
				cepAdm.createEPL("                                                           "
				//+ "context incrementByChannel "
				+ "insert into CountByChannelEvent "
				+ "select (ce.userCount+1) as userCount, ce.userChannel as userChannel , ue.timeStamp as timeStamp "
				+ "from CountByChannelEvent.win:keepall() as ce, UserEvent(userAction='<vistaTV:join>').win:length(1) as ue  where ce.userChannel = ue.userChannel output last when terminated ");

		EPStatement initChanneltatement = 
				cepAdm.createEPL("                                                           "
				//+ "context incrementByChannel "
				+ "insert into CountByChannelEvent "
				+ "select 1L as userCount, ue.userChannel as userChannel , ue.timeStamp as timeStamp "
				+ "from UserEvent(userAction='<vistaTV:join>').win:length(1) as ue "
				+ "Where not exists  "
				+ "( "
				+ "select *  "
				+ "from CountByChannelEvent.win:keepall() as ce where ce.userChannel = ue.userChannel "
				+ ") "
				);
		//EPStatement userCountEventLogStatement = 
		//				cepAdm.createEPL("select * from CountByChannelEvent");
				

		EPStatement decrementByOneStatement = 
				cepAdm.createEPL("                                                           "
				+ "context incrementByChannel "
						+ "insert into CountByChannelEvent "
				+ "select (ce.userCount-1) as userCount, ce.userChannel as userChannel , ue.timeStamp as timeStamp "
				+ "from CountByChannelEvent.win:keepall() as ce, UserEvent(userAction='<vistaTV:quit>').win:length(1) as ue  where ce.userChannel = ue.userChannel output last when terminated ");

		
		//EPStatement userEventLogStatement = 
		//		cepAdm.createEPL("select * from UserEvent");
		
		EPStatement countEventLogStatement = 
				cepAdm.createEPL("select current_timestamp as currentTime,  userCount,  userChannel from CountByChannelEvent.win:keepall().std:unique(userChannel) group by userChannel output snapshot every "+ windowSize+" sec");
		
				//cepAdm.createEPL("select distinct channel  from CountByChannelEvent.win:keepall().std:unique(userChannel) output snapshot every "+ windowSize+" sec");
				//cepAdm.createEPL("select * from CountByChannelEvent ");
		
		//setStatementSubscriber(incrementByOneStatement, "incrementByOneLog", isPrinting);
		//setStatementSubscriber(decrementByOneStatement, "decrementByOneLog", isPrinting);
		//setStatementSubscriber(initChanneltatement, "initChanneltatement", isPrinting);
		//setStatementSubscriber(userCountEventLogStatement, "userCountEventLogStatement", isPrinting);
		
		setStatementSubscriber(countEventLogStatement, "countUserResults", isPrinting);
	}
	
}
