package ch.uzh.ifi.ddis.ifp.algebra.impl;

import java.text.SimpleDateFormat;
import java.util.Map;


import com.espertech.esper.event.map.MapEventBean;

public class Utilities {

	public static double parserXSDDouble(String XSDString) {
		return Double.parseDouble(XSDString.substring(XSDString.indexOf("\"")+1, XSDString.indexOf("^")-1));
	}
	public static Long parserXSDLong(String XSDString) {
		return Long.parseLong(XSDString.substring(XSDString.indexOf("\"")+1, XSDString.indexOf("^")-1));
	}
	public static String printResultEvent(MapEventBean resultEvent) {
		String outputString = "";
		for(Map.Entry<String, Object> temp1: resultEvent.getProperties().entrySet()) {
			outputString += temp1.getKey()  +" "+temp1.getValue().toString();
		}
		
		return outputString;
	}
	public static void printTimeStamp(Long unixTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(unixTime); 
		System.out.println(date);
	}
	public static String getTimeStamp(Long unixTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(unixTime); 
		return date;
	}
	
}
