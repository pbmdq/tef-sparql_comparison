package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.Node;
import ch.uzh.ifi.ddis.ifp.algebra.Op;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;

/**
 * 
 * @author Thomas Scharrenbach
 * 
 */
public abstract class BasicNode implements Node {

	String _RDFRole;

	public BasicNode(){
		_RDFRole = null;
	}
	@Override
	public boolean isVariable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isIRI() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isBlankNode() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public VariableNode asVariable() throws ClassCastException {
		// TODO Auto-generated method stub
		throw new ClassCastException(this.getClass()
				+ " cannot be casted to VARNode");
	}

	@Override
	public IRINode asIRI() throws ClassCastException {
		// TODO Auto-generated method stub
		throw new ClassCastException(this.getClass()
				+ " cannot be casted to VariableNode");
	}

	@Override
	public BlankNode asBlankNode() throws ClassCastException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LiteralNode asLiteral() throws ClassCastException {
		// TODO Auto-generated method stub
		/*
		throw new ClassCastException(this.getClass()
				+ " cannot be casted to LiteralNode");
		*/
		return (LiteralNodeImpl) this;
	}
	@Override
	public void setRDFRole(String role) {
		this._RDFRole = role;
	}
	@Override
	public String getRDFRole() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getFullName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getFullRDFName() {
		// TODO Auto-generated method stub
		return null;
	}
	


}
