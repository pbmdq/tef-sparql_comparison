package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.net.URI;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

import com.espertech.esper.client.soda.EPStatementObjectModel;
import com.espertech.esper.client.soda.PropertyValueExpression;
import com.espertech.esper.client.soda.SelectClauseElement;
import com.espertech.esper.client.soda.SelectClauseExpression;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.ColumnListNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.Node;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;
import ch.uzh.ifi.ddis.ifp.algebra.Clause;
/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
@SuppressWarnings("serial")
public class GroupByClause implements Serializable, Clause {
	
	private String _type;
	private Set<Node> _groupByAttributes;
	private Set<ListClause> _havingClauses;
	
	public GroupByClause (){
		_groupByAttributes = new LinkedHashSet<Node>();
		_havingClauses = new LinkedHashSet<ListClause>();
	}
	
	public void addGroupByAttribute(Node resultNode) {
		_groupByAttributes.add(resultNode);
	}
	public void addHavingClauses(ListClause havingClause) {
		_havingClauses.add(havingClause);
	}
	
	public void setGroupByAttributes(Set<Node> groupByAttributes) {
		_groupByAttributes = groupByAttributes;
	}
	public void setHavingClauses(Set<ListClause> havingClauses) {
		_havingClauses = havingClauses;
	}
	public Set<Node> getGroupByAttributes() {
		return _groupByAttributes;
	}
	public Set<ListClause> getHavingClauses() {
		return _havingClauses;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return _type;
	}

	@Override
	public String setType(String type) {
		_type = type;
		// TODO Auto-generated method stub
		return null;
	}
}
