package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.util.UUID;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
@SuppressWarnings("serial")
class BlankNodeImpl extends BasicNode implements BlankNode, Serializable {

	private final UUID _id;

	public BlankNodeImpl(UUID uuid) {
		if (uuid == null) {
			throw new NullPointerException();
		}
		_id = uuid;
	}

	@Override
	public boolean isBlankNode() {
		return true;
	}

	@Override
	public BlankNodeImpl asBlankNode() throws ClassCastException {
		return this;
	}
	
	@Override
	public Object getId() {
		return _id;
	}

	@Override
	public Class<?> getIdType() {
		return _id.getClass();
	}

	@Override
	public String toString() {
		return _id.toString();
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && (obj instanceof BlankNodeImpl)
				&& equals(((BlankNodeImpl) obj)._id);
	}

	@Override
	public int hashCode() {
		return _id.hashCode();
	}

	@Override
	public Object getObject() {
		// TODO Auto-generated method stub
		return _id;
	}

}
