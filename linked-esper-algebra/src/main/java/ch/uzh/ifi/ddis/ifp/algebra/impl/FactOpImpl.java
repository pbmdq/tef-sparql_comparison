package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.ColumnListNode;
import ch.uzh.ifi.ddis.ifp.algebra.FactOp;
import ch.uzh.ifi.ddis.ifp.algebra.Op;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitor;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;
import ch.uzh.ifi.ddis.ifp.algebra.TriplePattern;


public class FactOpImpl extends AbstractOp implements FactOp {
	
	private static Set<ColumnListNode> _factColumns;
	
	private final Set<SinceUntilOpImpl> _sinceUntil;

	private final Set<ReplaceOnOpImpl> _replaceOn;

	private final Set<Op> _ops;
	
	private static String _factName; 

	public FactOpImpl(Set<Op> ops) {
		// TODO Auto-generated constructor stub
		_ops = new LinkedHashSet<Op>();
		_sinceUntil = new LinkedHashSet<SinceUntilOpImpl>();
		_replaceOn = new LinkedHashSet<ReplaceOnOpImpl>();
		_factColumns = new LinkedHashSet<ColumnListNode>();
		
		this.setType("Fact");
		for(Op op : ops) {
			add(op);
		}
	}
	
	
	/**
	 * 
	 * @param op
	 * @return if adding to the respective sets was successful.
	 * @throws IllegalArgumentException
	 *             in case <code>op</code> is not of an appropriate type.
	 */
	public boolean add(Op op) {
		if (op instanceof SinceUntilOpImpl) {
			return _sinceUntil.add((SinceUntilOpImpl) op)
					& _ops.add(op);
		} else {
			throw new IllegalArgumentException();
		}
	}

	

	@Override
	public void visit(OpVisitor visitor) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object visit(OpVisitorReturn<?> visitor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Op> getOps() {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public Set<SinceUntilOpImpl> getSinceUntilOps() {
		// TODO Auto-generated method stub
		return _sinceUntil;
	}

	@Override
	public Set<ReplaceOnOpImpl> getReplaceOnOps() {
		// TODO Auto-generated method stub
		return _replaceOn;
	}


	public String getFactName() {
		return _factName;
	}


	public static void setFactName(String factName) {
		FactOpImpl._factName = factName;
	}


	public Set<ColumnListNode> getFactColumns() {
		return _factColumns;
	}
	public void setFactColumns(Set<ColumnListNode> inputTpSet) {
		_factColumns = inputTpSet;
	}

}