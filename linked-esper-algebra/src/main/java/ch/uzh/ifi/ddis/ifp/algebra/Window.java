package ch.uzh.ifi.ddis.ifp.algebra;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class Window {
	public Window(String type, String value){
		this._type = type;
		this._value = value;
		
	}

	public void setType(String type) {
		this._type = type;
	}

	public void setValue(String value) {
		this._value = value;
	}
	
	public void setUnit(String unit) {
		this._unit = unit;
	}
	
	public String getType() {
		return _type;
	}

	public String getValue() {
		return _value;
	}

	public String getUnit() {
		return _unit;
	}
	
	private String _type;

	private String _value;
	
	private String _unit;
}
