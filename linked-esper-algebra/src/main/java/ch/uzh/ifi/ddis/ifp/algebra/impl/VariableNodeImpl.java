package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;

@SuppressWarnings("serial")
class VariableNodeImpl extends BasicNode implements VariableNode, Serializable {

	private final String _name;
	private String _prefix;
	
	VariableNodeImpl(String prefix, String variableName) {
		_name 	= variableName;
		_prefix = prefix;
	}

	@Override
	public boolean isVariable() {
		return true;
	}

	@Override
	public VariableNodeImpl asVariable() {
		return this;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		return obj != null && (obj instanceof VariableNodeImpl)
				&& equals(((VariableNodeImpl) obj)._name);
	}

	public void setPrefix(String prefix){
		_prefix = prefix;
	}
	
	public String getPrefix(){
		return _prefix;
	}
	
	@Override
	public String getName() {
		return _name.substring(1);
	}
	@Override
	public String getFullName() {
		String bastName;
		if(_name.contains("?"))
			bastName = _name.substring(1);
		else
			bastName = _name;
		if(_prefix ==  null)
			return bastName;
		return _prefix+"."+bastName;
	}

	@Override
	public String toString() {
		return _name.substring(1);
	}
	
	public String getFullRDFName() {
		if(_prefix ==  null)
			return _RDFRole;
		return _prefix+"."+_RDFRole;
	}

	@Override
	public Object getObject() throws ClassCastException{
		// TODO Auto-generated method stub
		throw new ClassCastException(this.getClass()
				+ " cannot return object value");
		//return null;
	}
	
}
