package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.espertech.esper.client.soda.ArithmaticExpression;
import com.espertech.esper.client.soda.AvgProjectionExpression;
import com.espertech.esper.client.soda.ConstantExpression;
import com.espertech.esper.client.soda.EPStatementObjectModel;
import com.espertech.esper.client.soda.Expression;
import com.espertech.esper.client.soda.Expressions;
import com.espertech.esper.client.soda.PropertyValueExpression;
import com.espertech.esper.client.soda.RelationalOpExpression;
import com.espertech.esper.client.soda.SelectClauseElement;
import com.espertech.esper.client.soda.SelectClauseExpression;
import com.espertech.esper.client.soda.SumProjectionExpression;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.ColumnListNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.Node;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;
import ch.uzh.ifi.ddis.ifp.algebra.Clause;

/**
 * <p>
 * This is a general clause implementation for where and set
 * </p>
 * 
 * @author Shen Gao
 * 
 */
@SuppressWarnings("serial")
public class ListClause implements Serializable, Clause {
	Node _leftNode;

	String _leftAggFunction;

	Map<Node, String> _rightExpressions = new LinkedHashMap<Node, String>();

	public ListClause() {
	}

	public String getLeftAggFunction() {
		return _leftAggFunction;
	}

	public void setLeftAggFunction(String _leftAggFunction) {
		this._leftAggFunction = _leftAggFunction;
	}

	public Node getLeftNode() {
		return _leftNode;
	}

	public void setLeftNode(Node _leftNode) {
		this._leftNode = _leftNode;
	}

	public Map<Node, String> getRightExpressions() {
		return _rightExpressions;
	}

	public void setRightExpressions(Map<Node, String> rightExpressions) {
		this._rightExpressions = rightExpressions;
	}

	public Expression toExpression() {
		Entry<Node, String> firstEntry = _rightExpressions.entrySet()
				.iterator().next();
		String firstOperator = firstEntry.getValue();
		Node firstNode = firstEntry.getKey();

		if (firstOperator.equalsIgnoreCase("=")) {
			/*
			 * return new RelationalOpExpression(Expressions.cast(new
			 * PropertyValueExpression(this .getLeftNode().getFullName()),
			 * "String"), "=", Expressions.cast(new
			 * PropertyValueExpression(firstNode.getFullName()), "String"));
			 */
			return Expressions.eq(new PropertyValueExpression(this
					.getLeftNode().getFullName()), new PropertyValueExpression(
					firstNode.getFullName()));
		}

		if (firstOperator.equalsIgnoreCase(">")
				|| firstOperator.equalsIgnoreCase("<")
				|| firstOperator.equalsIgnoreCase("!=")) {
			return new RelationalOpExpression(new PropertyValueExpression(this
					.getLeftNode().getFullName()), firstOperator,
					new PropertyValueExpression(firstNode.getFullName()));

			/*
			 * return new RelationalOpExpression(new
			 * PropertyValueExpression(this .getLeftNode().getFullName()),
			 * firstOperator, Expressions.constant("test"));
			 */
		}

		return null;
	}
	public Expression nodeToExpression ( Node inputNode){
		if (inputNode.isIRI()) {
			return Expressions.constant(
					(inputNode.asLiteral()).getValue(),
					(inputNode.asLiteral()).getValueType());// firstNode.asLiteral().getFullName();
		} else if (inputNode.isVariable())
			return new PropertyValueExpression(inputNode
					.asVariable().getFullName());
		return null;
	}

	public Expression toExpression(boolean isSetExpression) {
		if (isSetExpression) {

			Entry<Node, String> firstEntry = _rightExpressions.entrySet()
					.iterator().next();
			String firstOperator = firstEntry.getValue();
			Node firstNode = firstEntry.getKey();

			// TODO right expression to finiallize

			if (firstOperator.equalsIgnoreCase(">")
					|| firstOperator.equalsIgnoreCase("<")
					|| firstOperator.equalsIgnoreCase("!=")
					|| firstOperator.equalsIgnoreCase("=")) {
				Expression rightExpression = null;

				rightExpression = nodeToExpression(firstNode);
				
				if (_rightExpressions.size() > 1) {
					ArithmaticExpression tempArithmaticExpression = null;
					Node[] keyArray = _rightExpressions.keySet().toArray(
							new Node[0]);
					String[] valueArray = _rightExpressions.values().toArray(
							new String[0]);
					for (int i = 1; i < keyArray.length; i++) {
						tempArithmaticExpression = new ArithmaticExpression(
								rightExpression, valueArray[i], nodeToExpression(keyArray[i]));
						rightExpression = tempArithmaticExpression;
					}
				}

				// if()

				return new RelationalOpExpression(new PropertyValueExpression(
						this.getLeftNode().getFullName()), firstOperator,
						rightExpression);

				/*
				 * return new RelationalOpExpression(new
				 * PropertyValueExpression(this .getLeftNode().getFullName()),
				 * firstOperator, Expressions.constant("test"));
				 */
			}

		}
		return null;
	}

	public Expression toExpression(EPStatementObjectModel resultQuery) {
		Entry<Node, String> firstEntry = _rightExpressions.entrySet()
				.iterator().next();
		String firstOperator = firstEntry.getValue();
		Node firstNode = firstEntry.getKey();
		Expression leftExpression = null;
		Expression rightExpression = null;

		for (SelectClauseElement tempSelect : resultQuery.getSelectClause()
				.getSelectList()) {
			if (((SelectClauseExpression) tempSelect).getAsName()
					.equalsIgnoreCase(_leftNode.getFullName())) {
				SelectClauseExpression sec = (SelectClauseExpression) tempSelect;

				leftExpression = ((PropertyValueExpression) sec.getExpression());

				// leftExpression.s
				// leftExpression.s
				// leftExpression = new PropertyValueExpression("tp_0.subject");
				// resultQuery.get
				if (_leftAggFunction != null) {
					if (_leftAggFunction.equalsIgnoreCase("AVG"))
						leftExpression = new AvgProjectionExpression(
								new PropertyValueExpression(
										((PropertyValueExpression) leftExpression)
												.getPropertyName()
												+ ".doubleValue"), false);
					else if (_leftAggFunction.equalsIgnoreCase("SUM"))
						leftExpression = new SumProjectionExpression(
								new PropertyValueExpression(
										((PropertyValueExpression) leftExpression)
												.getPropertyName()
												+ ".doubleValue"), false);
				}

				// leftExpression = Expressions.p
			}
		}
		if (leftExpression == null) {
			leftExpression = new PropertyValueExpression(this.getLeftNode()
					.getFullName());

		}

		for (SelectClauseElement tempSelect : resultQuery.getSelectClause()
				.getSelectList()) {
			if (((SelectClauseExpression) tempSelect).getAsName()
					.equalsIgnoreCase(firstNode.getFullName())) {
				SelectClauseExpression sec = (SelectClauseExpression) tempSelect;
				rightExpression = (PropertyValueExpression) sec.getExpression();

			}
		}
		if (rightExpression == null) {
			// firstNode.asLiteral().getValue()
			// firstNode.asLiteral().getValue().getClass()
			if (firstNode.isIRI()) {
				System.out.println(firstNode.asLiteral().toString());
				if (firstNode.asLiteral().toString().contains("float")) {
					rightExpression = Expressions.constant(Utilities
							.parserXSDDouble(firstNode.asLiteral().toString()),
							Double.class);
				} else
					rightExpression = Expressions
							.constant(firstNode.toString());
				// System.out.println(leftExpression.set);
			}
		}

		if (firstOperator.equalsIgnoreCase("=")
				|| firstOperator.equalsIgnoreCase(">")
				|| firstOperator.equalsIgnoreCase("<")) {
			RelationalOpExpression temp = new RelationalOpExpression(
					leftExpression, firstOperator, rightExpression);

			return temp;
		}

		return null;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String setType(String type) {
		// TODO Auto-generated method stub
		return null;
	}

}
