package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;

import ch.uzh.ifi.ddis.ifp.algebra.BasicGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.FilterOp;
import ch.uzh.ifi.ddis.ifp.algebra.GroupGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.Op;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitor;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;
import ch.uzh.ifi.ddis.ifp.algebra.TriplePattern;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;

/**
 * <p>
 * A {@link GroupGraphPatternOpImpl} can hold {@link BasicGraphPatternOpImpl}
 * objects, {@link FilterOpImpl} objects and other
 * {@link GroupGraphPatternOpImpl} objects.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
class GroupGraphPatternOpImpl extends AbstractOp implements
		GroupGraphPatternOp {

	private final Set<GroupGraphPatternOpImpl> _subGraphPatterns;

	private final Set<BasicGraphPatternOpImpl> _basicGraphPatterns;

	private Set<WhereNode> _filters;

	private final Set<Op> _ops;

	public GroupGraphPatternOpImpl(Set<Op> ops) {
		_ops = new LinkedHashSet<Op>();
		_subGraphPatterns = new LinkedHashSet<GroupGraphPatternOpImpl>();
		_basicGraphPatterns = new LinkedHashSet<BasicGraphPatternOpImpl>();
		_filters = new LinkedHashSet<WhereNode>();
		this.setType("GGP");
		for(Op op : ops) {
			add(op);
		}
		this.getId();
		for (BasicGraphPatternOp bgp : this.getBasicGraphPatterns()) {
			for (String key : bgp.getVairableNodes().keySet()) {
				bgp.getVairableNodes().get(key).setPrefix(bgp.getId().toString());
				//System.out.println(bgp.getVairableNodes().get(key).getFullName());
			}
		}
	}

	@Override
	public void visit(OpVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Object visit(OpVisitorReturn<?> visitor) {
		return visitor.visit(this);
	}

	/**
	 * 
	 * @param op
	 * @return if adding to the respective sets was successful.
	 * @throws IllegalArgumentException
	 *             in case <code>op</code> is not of an appropriate type.
	 */
	public boolean add(Op op) {
		if (op instanceof GroupGraphPatternOpImpl) {
			return _subGraphPatterns.add((GroupGraphPatternOpImpl) op)
					& _ops.add(op);
		} else if (op instanceof BasicGraphPatternOpImpl) {
			return _basicGraphPatterns.add((BasicGraphPatternOpImpl) op)
					& _ops.add(op);
		} else {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * 
	 * @return fresh {@link LinkedHashSet}.
	 */
	public Set<Op> getOps() {
		return new LinkedHashSet<Op>(_ops);
	}

	/**
	 * 
	 * @return fresh {@link LinkedHashSet}.
	 */
	public Set<GroupGraphPatternOp> getSubGraphPatterns() {
		return new LinkedHashSet<GroupGraphPatternOp>(_subGraphPatterns);
	}

	/**
	 * 
	 * @return fresh {@link LinkedHashSet}.
	 */
	public Set<BasicGraphPatternOp> getBasicGraphPatterns() {
		return new LinkedHashSet<BasicGraphPatternOp>(_basicGraphPatterns);
	}

	@Override
	public Set<WhereNode> getFilters() {
		// TODO Auto-generated method stub
		return _filters;
	}
	public void setFilters( Set<WhereNode> filters) {
		// TODO Auto-generated method stub
		_filters = filters;
	}

	public Map<String, Collection<String>> getCommonVariable() {
		Map<String, Collection<String>> commonVariables = new HashMap<String, Collection<String>>();
		// "%s.%s", tp.getResultEventName(), SELECT_SUBJECT
		if (this.getBasicGraphPatterns().size() > 1) {
			for (BasicGraphPatternOp bgp : this.getBasicGraphPatterns()) {
				for (String key : bgp.getVairableNodes().keySet()) {
					if (commonVariables.get(key) == null) {
						commonVariables.put(key, new ArrayList<String>());
						bgp.getVairableNodes().get(key).setPrefix(bgp.getId().toString());
						commonVariables.get(key).add(
								bgp.getVairableNodes().get(key).getFullName());
						System.out.println(bgp.getVairableNodes().get(key).getFullName());
					} else {
						bgp.getVairableNodes().get(key).setPrefix(bgp.getId().toString());
						commonVariables.get(key).add(
								bgp.getVairableNodes().get(key).getFullName());
						System.out.println(bgp.getVairableNodes().get(key).getFullName());
					}
				}
			}
		}
		return commonVariables;
	}
}


