package ch.uzh.ifi.ddis.ifp.algebra;


/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * <p>
 * Simple struct holding a subject, a predicate and an object node.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class TriplePattern {

	public TriplePattern(Node subject, Node predicate, Node object) {
		if (subject == null) {
			throw new NullPointerException();
		}
		if (predicate == null) {
			throw new NullPointerException();
		}
		if (object == null) {
			throw new NullPointerException();
		}
		this._subject = subject;
		_subject.setRDFRole("subject");
		this._predicate = predicate;
		_predicate.setRDFRole("predicate");
		this._object = object;
		_object.setRDFRole("object");
				
	}

	public Node getSubject() {
		return _subject;
	}

	public Node getPredicate() {
		return _predicate;
	}

	public Node getObject() {
		return _object;
	}
	
	public String getResultEventName() {
		return _resultEventName;
	}
	
	public void setResultEventName( String ResultEventName ) {
		_resultEventName = ResultEventName;
		if(_subject.isVariable())
			_subject.asVariable().setPrefix(ResultEventName);
		if(_predicate.isVariable())
			_predicate.asVariable().setPrefix(ResultEventName);
		if(_object.isVariable())
			_object.asVariable().setPrefix(ResultEventName);
	}

	private final Node _subject;

	private final Node _predicate;

	private final Node _object;
	
	private String _resultEventName;

}
