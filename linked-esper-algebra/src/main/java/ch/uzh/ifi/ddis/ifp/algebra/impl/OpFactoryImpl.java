package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.BasicGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.FactOp;
import ch.uzh.ifi.ddis.ifp.algebra.FilterOp;
import ch.uzh.ifi.ddis.ifp.algebra.GroupGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.Op;
import ch.uzh.ifi.ddis.ifp.algebra.OpFactory;
import ch.uzh.ifi.ddis.ifp.algebra.ProjectionOp;
import ch.uzh.ifi.ddis.ifp.algebra.TriplePattern;
import ch.uzh.ifi.ddis.ifp.algebra.Window;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class OpFactoryImpl implements OpFactory {

	@Override
	public GroupGraphPatternOp createGroupGraphPattern(Set<Op> ops) {
		return new GroupGraphPatternOpImpl(ops);
	}

	@Override
	public FilterOp createFilter(Object filterExpression) {
		if (filterExpression == null) {
			throw new NullPointerException();
		}
		return new FilterOpImpl(filterExpression);
	}

	@Override
	public BasicGraphPatternOp createBasicGraphPattern(
			Set<TriplePattern> triplePatterns, Window win) {
		return new BasicGraphPatternOpImpl(triplePatterns, win);
	}

	@Override
	public ProjectionOp createProjectionOp(Set<String> properties, Op op) {
		if (properties == null) {
			throw new NullPointerException();
		}
		if (op == null) {
			throw new NullPointerException();
		}
		return new ProjectionOpImpl(properties, op);
	}
	
	@Override
	public FactOp crateFactOp(Set<Op> ops) {
		return new FactOpImpl(ops);
	}
	

}
