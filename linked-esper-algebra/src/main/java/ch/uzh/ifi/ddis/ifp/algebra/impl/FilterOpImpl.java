package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.ddis.ifp.algebra.FilterOp;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitor;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;

/**
 * <p>
 * Simple value holder for a filter expression.
 * </p>
 * <p>
 * Filters are not yet implemented.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
class FilterOpImpl extends AbstractOp implements FilterOp {

	private Object _filterExpression;

	public FilterOpImpl(Object filterExpression) {

	}

	@Override
	public void visit(OpVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Object visit(OpVisitorReturn<?> visitor) {
		return visitor.visit(this);
	}

	public Object getFilterExpression() {
		return _filterExpression;
	}

}
