package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.Op;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitor;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;
import ch.uzh.ifi.ddis.ifp.algebra.ProjectionOp;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class ProjectionOpImpl extends AbstractOp implements ProjectionOp {

	private final Set<String> _properties;
	
	private final Op _op;

	public ProjectionOpImpl(Set<String> properties, Op op) {
		_properties = new LinkedHashSet<String>(properties);
		_op = op;
		this.setType("PROJECT");
	}

	@Override
	public void visit(OpVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Object visit(OpVisitorReturn<?> visitor) {
		return visitor.visit(this);
	}

	@Override
	public Set<String> getProperties() {
		return new LinkedHashSet<String>(_properties);
	}
	
	@Override
	public Op getOp() {
		return _op;
	}

}
