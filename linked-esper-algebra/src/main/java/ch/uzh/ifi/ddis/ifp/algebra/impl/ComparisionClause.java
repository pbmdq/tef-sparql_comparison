package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.net.URI;
import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.ColumnListNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;
import ch.uzh.ifi.ddis.ifp.algebra.Clause;
/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
@SuppressWarnings("serial")
public class ComparisionClause implements Serializable, Clause {
	
	BasicNode _leftNode;
	BasicNode _rightNode;
	String _comparisionOperator;
	
	public ComparisionClause(){
		
	}
	public ComparisionClause(BasicNode leftNode, String comparisionOperator, BasicNode rightNode){
		setComparisionOperator(comparisionOperator);
		setLeftNode(leftNode);
		setRigthNode(rightNode);
	}
	
	public BasicNode getLeftNode() {
		return _leftNode;
	}
	public void setLeftNode(BasicNode leftNode) {
		this._leftNode = leftNode;
	}
	public BasicNode getRigthNode() {
		return _rightNode;
	}
	public void setRigthNode(BasicNode rightNode) {
		this._rightNode = rightNode;
	}
	public String getComparisionOperator() {
		return _comparisionOperator;
	}
	public void setComparisionOperator(String _comparisionOperator) {
		this._comparisionOperator = _comparisionOperator;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String setType(String type) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
