package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedHashSet;
import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.ExpressionNode;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitor;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;
import ch.uzh.ifi.ddis.ifp.algebra.TriplePattern;
import ch.uzh.ifi.ddis.ifp.algebra.SinceUntilOp;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;


public class SinceUntilOpImpl extends AbstractOp implements SinceUntilOp {
	Set<TriplePattern> _triplePatterns;
	String _onEventName;
	Set<ListClause> _whereClauses;
	
	public SinceUntilOpImpl(Set<TriplePattern> triplePatterns, String onEventName) {
		_triplePatterns = new LinkedHashSet<TriplePattern>(triplePatterns);
		_whereClauses	= new LinkedHashSet<ListClause>();
		setOnEventName(onEventName);
	}
	
	@Override
	public void visit(OpVisitor visitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object visit(OpVisitorReturn<?> visitor) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getOnEventName() {
		return _onEventName;
	}

	public void setOnEventName(String _onEventName) {
		this._onEventName = _onEventName;
	}

	public Set<ListClause> getWhereClauses() {
		return _whereClauses;
	}

	public void setWhereClauses(Set<ListClause> resultClauses) {
		this._whereClauses = resultClauses;
	}


}
