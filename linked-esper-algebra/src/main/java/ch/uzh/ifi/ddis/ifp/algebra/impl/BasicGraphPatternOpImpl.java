package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.BasicGraphPatternOp;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitor;
import ch.uzh.ifi.ddis.ifp.algebra.OpVisitorReturn;
import ch.uzh.ifi.ddis.ifp.algebra.TriplePattern;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;
import ch.uzh.ifi.ddis.ifp.algebra.Window;


/**
 * <p>
 * A basic graph pattern comprises a set of {@link TriplePattern} objects.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class BasicGraphPatternOpImpl extends AbstractOp implements BasicGraphPatternOp {

	private Set<TriplePattern> _triplePatterns;

	private Window _win;
	
	private Set<WhereNode> _filters;
	
	public static final String SELECT_SUBJECT = "subject";
	public static final String SELECT_PREDICATE = "predicate";
	public static final String SELECT_OBJECT = "object";
	

	public BasicGraphPatternOpImpl(Set<TriplePattern> triplePatterns, Window win) {
		_triplePatterns = new LinkedHashSet<TriplePattern>();
		_win = win;
		this.setType("BGP");
		_filters = new LinkedHashSet<WhereNode>();
	}

	public Set<TriplePattern> getTriplePatterns() {
		return new LinkedHashSet<TriplePattern>(_triplePatterns);
	}
	public void setTriplePatterns( Set<TriplePattern> triplePatterns) {
		_triplePatterns = triplePatterns;
	}

	public boolean isEmpty() {
		return _triplePatterns.isEmpty();
	}

	@Override
	public void visit(OpVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Object visit(OpVisitorReturn<?> visitor) {
		return visitor.visit(this);
	}

	public int getTriplePatternSize() {
		return _triplePatterns.size();
	}

	public Window getWindow() {
		return _win;
	}
	public void setWindow(Window win) {
		_win = win;
	}

	/**
	 * 
	 * @return fresh {@link LinkedHashSet}.
	 */
	public Set<WhereNode> getFilters() {
		return _filters;
	}
	public void setFilters( Set<WhereNode> filters) {
		_filters = filters;
	}
	// get common variable for bgp
	public Map<String, Collection<String>> getCommonVariable() {
		Map<String, Collection<String>> commonVariables = new HashMap<String, Collection<String>>();
		// "%s.%s", tp.getResultEventName(), SELECT_SUBJECT
		if (this.getTriplePatternSize() > 1) {
			for (TriplePattern tp : this.getTriplePatterns()) {
				if (tp.getSubject().isVariable()) {
					if (commonVariables.get(tp.getSubject().toString()) == null) {
						commonVariables.put(tp.getSubject().toString(),
								new ArrayList<String>());
						commonVariables.get(tp.getSubject().toString()).add(
								String.format("%s.%s", tp.getResultEventName(),
										SELECT_SUBJECT));
					} else {
						commonVariables.get(tp.getSubject().toString()).add(
								String.format("%s.%s", tp.getResultEventName(),
										SELECT_SUBJECT));
					}
					//_log.debug("put in subject'{}'", String.format("%s.%s",
					//		tp.getResultEventName(), SELECT_SUBJECT));

				}
				if (tp.getPredicate().isVariable()) {
					if (commonVariables.get(tp.getPredicate().toString()) == null) {
						commonVariables.put(tp.getPredicate().toString(),
								new ArrayList<String>());
						commonVariables.get(tp.getPredicate().toString()).add(
								String.format("%s.%s", tp.getResultEventName(),
										SELECT_PREDICATE));
					} else
						commonVariables.get(tp.getPredicate().toString()).add(
								String.format("%s.%s", tp.getResultEventName(),
										SELECT_PREDICATE));
					//_log.debug("put in subject'{}'", String.format("%s.%s",
					//		tp.getResultEventName(), SELECT_PREDICATE));

				}
				if (tp.getObject().isVariable()) {
					if (commonVariables.get(tp.getObject().toString()) == null) {
						commonVariables.put(tp.getObject().toString(),
								new ArrayList<String>());
						commonVariables.get(tp.getObject().toString()).add(
								String.format("%s.%s", tp.getResultEventName(),
										SELECT_OBJECT));
					} else
						commonVariables.get(tp.getObject().toString()).add(
								String.format("%s.%s", tp.getResultEventName(),
										SELECT_OBJECT));
				}
				//_log.debug("put in object'{}'", String.format("%s.%s",
				//		tp.getResultEventName(), SELECT_OBJECT));
			}
		}
		return commonVariables;
	}

	@Override
	public Map<String, VariableNode> getVairableNodes() {
		// TODO Auto-generated method stub
		Map<String, VariableNode> commonVariables = new HashMap<String, VariableNode>();
		for (TriplePattern tp : this.getTriplePatterns()) {
			if (tp.getSubject().isVariable()) {
				if (commonVariables.get(tp.getSubject().toString()) == null) {
					commonVariables.put(tp.getSubject().toString(),
							(VariableNode) tp.getSubject());
				}
				if (tp.getPredicate().isVariable()) {
					if (commonVariables.get(tp.getPredicate().toString()) == null) {
						commonVariables.put(tp.getPredicate().toString(),
								(VariableNode) tp.getPredicate());
					}
				}
				if (tp.getObject().isVariable()) {
					if (commonVariables.get(tp.getObject().toString()) == null) {
						commonVariables.put(tp.getObject().toString(),
								(VariableNode) tp.getObject());
					}
				}
			}
		}
		return commonVariables;
	}

}
