package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.net.URI;
import java.util.UUID;

import ch.uzh.ifi.ddis.ifp.algebra.ColumnListNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.NodeFactory;
import ch.uzh.ifi.ddis.ifp.algebra.WhereNode;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class NodeFactoryImpl implements NodeFactory {

	@Override
	public BlankNodeImpl createBlankNode() {
		return new BlankNodeImpl(UUID.randomUUID());
	}

	@Override
	public BlankNodeImpl createBlankNode(String bnodeId) {
		if(bnodeId == null) {
			throw new NullPointerException();
		}
		try {
			new URI(bnodeId).toURL();
		} catch (NullPointerException e) {
			throw e;
		} catch (Exception e) {
			// No IRI, so we have to check for a variable name.
			if (bnodeId.startsWith("?")) {
				throw new IllegalArgumentException(
						"Blank node identifier must not start with a '?'!");
			}
			// Neither IRI nor variable name, so we have a valid value for the
			// blank node.
			return new BlankNodeImpl(UUID.fromString(bnodeId));
		}
		throw new IllegalArgumentException(
				"Blank node identifier must not be an IRI!");

		
	}

	@Override
	public IRINodeImpl createIRINode(String iri) {
		return new IRINodeImpl(iri);
	}
	

	@Override
	public VariableNodeImpl createVariableNode(String prefix, String variableName) {
		if(variableName == null) {
			throw new NullPointerException();
		}
		if(variableName.isEmpty()) {
			throw new IllegalArgumentException();
		}

		try {
			new URI(variableName).toURL();
		} catch (NullPointerException e) {
			throw e;
		} catch (Exception e) {
			// No IRI, so we have to check for a variable name.
			/*
			if (variableName.startsWith("?")) {
				throw new IllegalArgumentException(
						"Variable name must not start with a '?'!");
			}
			*/
			// Neither IRI nor variable name, so we have a valid value for the
			// blank node.
			return new VariableNodeImpl(prefix, variableName);
		}
		throw new IllegalArgumentException(String.format(
				"Variable name '%s' must not be an IRI!", variableName));
	}

	@Override
	public LiteralNode createLiteralNode(Object value) {
		return new LiteralNodeImpl(value);
	}
	@Override
	public ColumnListNode createColumnListNode(String name, String type) {
		return new ColumnListNodeImpl(name, type);
	}
	/*
	@Override
	public WhereNode createWhereNode(String leftN, String leftP, String operator, String rightN, String rightP) {
		return new WhereNodeImpl(leftN, leftP, operator, rightN, rightP);
	}
	 */
	@Override
	public IRINode createIRINode(URI iri) {
		// TODO Auto-generated method stub
		return null;
	}
}
