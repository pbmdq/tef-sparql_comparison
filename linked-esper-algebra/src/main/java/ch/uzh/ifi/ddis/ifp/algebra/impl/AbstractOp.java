package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import ch.uzh.ifi.ddis.ifp.algebra.Op;

/**
 * 
 * @author Thomas Scharrenbach
 * 
 */
public abstract class AbstractOp implements Op {

	private static Integer _globalId = 0;
	
	protected String _type = null;
	
	protected String _outputCache = null;
	
	protected GroupByClause _groupByClause = null;
	
	public GroupByClause getGroupByClause() {
		return _groupByClause;
	}

	public void setGroupByClause(GroupByClause _groupByClause) {
		this._groupByClause = _groupByClause;
	}

	private static Object createId() {
		int result = 0;
		synchronized (_globalId) {
			result = ++_globalId;
		}
		return result;
	}

	private Object _id;

	public AbstractOp() {
		_id = createId();
	}

	@Override
	public Object getId() {
		return "op_" + _id;
	}
	
	public String getType() {
		return _type;
	}
	
	public void setType(String type) {
		_type = type;
	}
	
	public String getOutputCache() {
		return _outputCache;
	}
	
	public void setOutputCache(String outputCache) {
		_outputCache = outputCache;
	}

	
	
}
