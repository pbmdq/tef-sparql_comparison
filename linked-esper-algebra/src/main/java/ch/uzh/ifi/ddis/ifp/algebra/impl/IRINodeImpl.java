package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.net.URI;

import ch.uzh.ifi.ddis.ifp.algebra.IRINode;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
@SuppressWarnings("serial")
class IRINodeImpl extends BasicNode implements IRINode, Serializable {

	private final String _iri;

	public IRINodeImpl(String iri) {
		if (iri == null) {
			throw new NullPointerException();
		}
		_iri = iri;
	}

	@Override
	public boolean isIRI() {
		return true;
	}

	@Override
	public IRINodeImpl asIRI() throws ClassCastException {
		return this;
	}

	@Override
	public String getIRI() {
		return _iri;
	}

	@Override
	public String toString() {
		return _iri.toString();
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && (obj instanceof IRINodeImpl)
				&& equals(((IRINodeImpl) obj)._iri);
	}

	@Override
	public int hashCode() {
		return _iri.hashCode();
	}

	@Override
	public Object getObject() {
		// TODO Auto-generated method stub
		return _iri;
	}

}
