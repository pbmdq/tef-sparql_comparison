package ch.uzh.ifi.ddis.ifp.algebra.impl;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;

import ch.uzh.ifi.ddis.ifp.algebra.BlankNode;
import ch.uzh.ifi.ddis.ifp.algebra.IRINode;
import ch.uzh.ifi.ddis.ifp.algebra.LiteralNode;
import ch.uzh.ifi.ddis.ifp.algebra.VariableNode;

/**
 * <p>
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
@SuppressWarnings("serial")
class LiteralNodeImpl extends BasicNode implements LiteralNode, Serializable {

	private final Object _value;

	public LiteralNodeImpl(Object value) {
		if (value == null) {
			throw new NullPointerException();
		}
		_value = value;
	}

	@Override
	public boolean isIRI() {
		return true;
	}

	@Override
	public LiteralNodeImpl asLiteral() throws ClassCastException {
		return this;
	}

	
	@Override
	public Class<?> getType() {
		return _value.getClass();
	}
	
	@Override
	public String toString() {
		return _value.toString();
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && (obj instanceof LiteralNodeImpl)
				&& equals(((LiteralNodeImpl) obj)._value);
	}

	@Override
	public Object getValue() {
		if(_value.toString().contains("long"))
			return Utilities.parserXSDLong((String)_value);
		if(_value.toString().contains("float"))
			return Utilities.parserXSDDouble((String)_value);
		return null;
	}
	
	@Override
	public Class<?> getValueType() {
		if(_value.toString().contains("long"))
			return Long.class;
		if(_value.toString().contains("float"))
			return Double.class;
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return _value.hashCode();
	}

	@Override
	public Object getObject() {
		// TODO Auto-generated method stub
		return _value;
	}
	

}
