// Define a grammar called Hello
grammar FunctionalAlgebra;

@parser::header {
import ch.uzh.ifi.ddis.ifp.algebra.*;
import ch.uzh.ifi.ddis.ifp.algebra.impl.*;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
}

@parser::members {
	private static final Logger _log = LoggerFactory
			.getLogger(FunctionalAlgebraParser.class);
	
	NodeFactory _nodeFactory 	= new NodeFactoryImpl();
	OpFactory _opFactory 		= new OpFactoryImpl();
	Stack<Op> _opStack 			= new Stack<Op>();
	Stack<TriplePattern> _tpStack = new Stack<TriplePattern>();
	Stack<Window> _windowStack 	= new Stack<Window>();
	Stack<Node> _nodeStack 		= new Stack<Node>();
	
	Stack<ExpressionNode> _setNodeStack = new Stack<ExpressionNode>();
	Stack<ExpressionNode> _setRightNodeStack = new Stack<ExpressionNode>();
	Stack<WhereNode> _whereNodeStack = new Stack<WhereNode>();
	List<VariableNode> _exprList= new ArrayList<VariableNode>();
	Set<ColumnListNode> _columnListSet= new LinkedHashSet<ColumnListNode>();
}

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                  GRAMMAR RULES                        */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Rule for a query.

query returns [Op expr]
:
	 (((project | ggp | bgp ) ( AS cache )?) | fact)
	{
		Op result = _opStack.pop();
		$expr=result;
	}
;

fact
@init 
{
	Set<Op> ops = new LinkedHashSet<Op>();
	Op currFact = _opFactory.crateFactOp(ops);
}
:
	 ((FACT LPAREN columnList RPAREN AS factname[currFact])|(FACT LPAREN factname[currFact] RPAREN )
		( (since[currFact])
			|(until[currFact])
			|(replace[currFact])
		)?)
	
	{
		((FactOpImpl) currFact).setFactColumns(_columnListSet);
		_log.debug("Parsing new Fact columns '{}'", currFact.getId());
		_opStack.push(currFact);
	}
;
columnList
:
	(columnListNameString = COLUMNLISTNAME typeString = TYPE) 
	{
		_columnListSet.add(_nodeFactory.createColumnListNode($columnListNameString.text, $typeString.text));
		_log.debug("Parsing new column name '{}' column type '{}'", $columnListNameString.text, $typeString.text);
	}
	(
	COMMA columnListNameString = COLUMNLISTNAME typeString = TYPE
	{
		_columnListSet.add(_nodeFactory.createColumnListNode($columnListNameString.text, $typeString.text));
		_log.debug("Parsing new column name '{}' column type '{}'", $columnListNameString.text, $typeString.text);
	}
	)*
;

factname[Op currFact]
:
	(factnameText = FACTNAME)
	{
		//Op result = _opStack.peek();
		((FactOpImpl) currFact).setFactName($factnameText.text);
		_log.debug("Parsing new Fact name '{}'", $factnameText.text);
		//result.setOutputCache($factnameText.text);
	}
;

cache
:
	(cacheName = CACHENAME)
	{
		Op result = _opStack.peek();
		result.setOutputCache($cacheName.text);
	}
;
// WHERE clausesSet = getClauses 
// since is now with out where clause
since[Op currFact]
:
	(LPAREN SINCE ON LPAREN faceNameString = CACHENAME  RPAREN WHERE clausesSet = getClauses RPAREN )
	{
		Set<TriplePattern> tpSet = new LinkedHashSet<TriplePattern>(_tpStack);
		_tpStack.clear();
		SinceUntilOpImpl tempSince 	= new SinceUntilOpImpl(tpSet, $faceNameString.text); 
		tempSince.setWhereClauses($clausesSet.resultClauses);
		tempSince.setType("SINCE");
		((FactOpImpl) currFact).add((Op)tempSince);
		_log.debug("Parsing new SinceOp '{}'", tempSince.getId());
	}
;
until[Op currFact]
:
	(LPAREN UNTIL ON LPAREN faceNameString = CACHENAME  RPAREN WHERE clausesSet = getClauses RPAREN )
	{
		Set<TriplePattern> tpSet = new LinkedHashSet<TriplePattern>(_tpStack);
		_tpStack.clear();
		SinceUntilOpImpl tempUntil 	= new SinceUntilOpImpl(tpSet, $faceNameString.text); 
		tempUntil.setWhereClauses($clausesSet.resultClauses);
		tempUntil.setType("UNTIL");
		((FactOpImpl) currFact).add((Op)tempUntil);
		_log.debug("Parsing new UntilOp '{}'", tempUntil.getId());
	}
	
;

replace[Op currFact]
:
	(LPAREN REPLACE ON LPAREN faceNameString = CACHENAME RPAREN WHERE WhereClausesSet = getClauses SET setClausesSet = getClauses RPAREN  )
	{
		_log.debug("in replace on ");
		Set<TriplePattern> tpSet = new LinkedHashSet<TriplePattern>(_tpStack);
		_tpStack.clear();
		ReplaceOnOpImpl tempSet 	= new ReplaceOnOpImpl(tpSet, $faceNameString.text); 
		
		tempSet.setWhereClauses($WhereClausesSet.resultClauses);
		tempSet.setSetClauses($setClausesSet.resultClauses);
		tempSet.setType("REPLACEON");
		
		((FactOpImpl) currFact).add((Op)tempSet);
		_log.debug("Parsing new ReplaceOn '{}'", tempSet.getId());
		_log.debug("Parsing new ReplaceOn");
	}
;

getClauses returns[Set<ListClause> resultClauses]
@init 
{
	$resultClauses = new HashSet<ListClause>();
}
:
	(oneClause[$resultClauses]) (AND oneClause[$resultClauses])*
;


oneClause[Set<ListClause> resultClauses] returns[ListClause rootClause]
@init 
{
	$rootClause = new ListClause();
}
:
 	(( aggregateFunction = AGGREGATEFUNCTION LPAREN)? leftNode = singleNode (RPAREN)? ) (rightExpression[$rootClause])+
 	{
 		$rootClause.setLeftAggFunction($aggregateFunction.text);
 		$rootClause.setLeftNode($leftNode.resultNode);
 		
 		resultClauses.add($rootClause);
 		// put into set
 	}
;
rightExpression[ListClause rootClause]
:
	( operator = OPERATOR ) ( tempNode = singleNode )
	{
		rootClause.getRightExpressions().put($tempNode.resultNode, $operator.text);
	}
;


singleNode returns[Node resultNode]
:
	(
		(inputName = (FACTNAME|CACHENAME) DOT)? inputProperty = COLUMNLISTNAME
		{
			_log.debug("Parsing new variable node '{}'", $inputProperty.text);
			$resultNode = _nodeFactory.createVariableNode($inputName.text, $inputProperty.text);
		}

		| value = IRI
		{
			_log.debug("Parsing new IRI node '{}'", $value.text);
			try {
				//_nodeStack.push(_nodeFactory.createIRINode(new java.net.URI($value.text.substring(1, $value.text.length()-1))));
				$resultNode = _nodeFactory.createIRINode($value.text.substring(0, $value.text.length()));
			
			} catch(Exception e) {
				throw new RuntimeException(e);
			}
		}
		| value = LIT
		{
			_log.debug("Parsing new literal node '{}'", $value.text);
			$resultNode = _nodeFactory.createLiteralNode($value.text);
		} 
		| (value = RDFLITERAL)
		{
			_log.debug("Parsing new literal float '{}'", $value.text);
			$resultNode = _nodeFactory.createLiteralNode($value.text);
		}
		| (value = CHAR)
		{
			_log.debug("Parsing new literal float '{}'", $value.text);
			$resultNode = _nodeFactory.createLiteralNode($value.text);
		}

	)
;

//
groupBy[Op currnetNode]
@init {
	GroupByClause groupByClause = new GroupByClause();
	boolean test = false;
}
:
	(DOT GROUPBY LPAREN groupbyAttributes[groupByClause]  RPAREN  ) ( HAVING  LPAREN havingClausesSet = getClauses{test = true;} RPAREN )?
	{
		_log.debug("group by adding new attribute");
		if( test == true )
			groupByClause.setHavingClauses($havingClausesSet.resultClauses);
		currnetNode.setGroupByClause(groupByClause);
	}
;
groupbyAttributes [GroupByClause groupByClause]
:
	((addBasicAttribute[groupByClause]) (COMMA addBasicAttribute[groupByClause])*)
;
addBasicAttribute[GroupByClause groupByClause]
:
	groupByAttributeNode = singleNode
	{
		groupByClause.addGroupByAttribute($groupByAttributeNode.resultNode);
		_log.debug("group by adding new attribute");
	}
;
  
project
:
	(PROJECT LPAREN exprList COMMA (ggp | bgp) RPAREN) 
	{
		Set<String> projectionPropertiesList = new LinkedHashSet<String>();
		for(VariableNode expr : _exprList) {
			projectionPropertiesList.add(expr.toString());
		}
		_exprList.clear();
		Op projectionOp = _opStack.pop();
		Op result = _opFactory.createProjectionOp(projectionPropertiesList, projectionOp);
		_log.debug("Parsing new projection pattern '{}'", result.getId());
		_opStack.push(result);
	}

;

exprList
:
	(value = COLUMNLISTNAME)
	{
		_exprList.add(_nodeFactory.createVariableNode(null, $value.text));
		_log.debug("Parsing new variable expression '{}'", $value.text);
	}

	(
		COMMA value = COLUMNLISTNAME
		{
			_exprList.add(_nodeFactory.createVariableNode(null, $value.text));
			_log.debug("Parsing new variable expression '{}'", $value.text);	
		}

	)*
;

/**
 * <p>
 * Group graph patterns parsing rule.
 * </p>
 */
ggp
:
	GGP LPAREN
	(
		ggp 
		| bgp
	)* RPAREN 
	{
		Set<Op> ops = new LinkedHashSet<Op>(_opStack);
		_opStack.clear();
		Op result = _opFactory.createGroupGraphPattern(ops);
		_log.debug("Parsing new group graph pattern '{}'", result.getId());
		_opStack.push(result);
	}

;
// end group graph patterns

bgp
@init {
	Window currWindow = new Window("keepall", null);
	//_windowStack.push(currWindow);
	Op currOp = _opFactory.createBasicGraphPattern(null, null);
		
}
:
	BGP LPAREN tp
	(
		COMMA tp
	)* RPAREN (win[currWindow] )? (filter)? (groupBy[currOp])?
	{
		Set<TriplePattern> tpSet = new LinkedHashSet<TriplePattern>(_tpStack);
		((BasicGraphPatternOpImpl)currOp).setTriplePatterns(tpSet);
		((BasicGraphPatternOpImpl)currOp).setWindow(currWindow);
		_tpStack.clear();
		Set<WhereNode> whereSet = new LinkedHashSet<WhereNode>(_whereNodeStack);
		_whereNodeStack.clear();
		((BasicGraphPatternOpImpl)currOp).setFilters(whereSet);
		_log.debug("Parsing new basic graph pattern '{}'", currOp.getId());
		_opStack.push(currOp);
	}

; // basic graph patterns

filter
:
	(DOT FILTER LPAREN filterClausesSet = getClauses RPAREN )
	{
		_log.debug("Parsing new filter");
	}
;
tp
:
	(TP LPAREN subject = node COMMA predicate = node COMMA object = node RPAREN )
	{
		Node objectNode 	= _nodeStack.pop();
		Node predicateNode 	= _nodeStack.pop();
		Node subjectNode 	= _nodeStack.pop();
		
		TriplePattern tp 	= new TriplePattern(subjectNode, predicateNode, objectNode);
		_log.debug("Parsing new triple pattern '{}'", tp);
		_tpStack.push(tp);
	}

; // triple patterns


//currWindow.setValue($winvalue1.text);
	
win[Window currWindow]
@after{
	currWindow.setType($winType.text);
	currWindow.setUnit($unitType.text);
	currWindow.setValue($winvalue1.text);
}
:
	(DOT WINDOW LPAREN winType = COLUMNLISTNAME (LPAREN  winvalue1 = CHAR ( unitType = UNITTYPE )? RPAREN)? RPAREN)
;



node
:
	(
		value = COLUMNLISTNAME
		{
			_log.debug("Parsing new variable node '{}'", $value.text);
			_nodeStack.push(_nodeFactory.createVariableNode(null, $value.text));
		}

		| value = IRI
		{
			_log.debug("Parsing new IRI node '{}'", $value.text);
			try {
				//_nodeStack.push(_nodeFactory.createIRINode(new java.net.URI($value.text.substring(1, $value.text.length()-1))));
				_nodeStack.push(_nodeFactory.createIRINode($value.text.substring(0, $value.text.length())));
			
			} catch(Exception e) {
				throw new RuntimeException(e);
			}
		}

		| value = LIT
		{
			_log.debug("Parsing new literal node '{}'", $value.text);
			_nodeStack.push(_nodeFactory.createLiteralNode($value.text));
		}

	)
;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                    LEXER RULES                        */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * */


PROJECT
:
	'PROJECT'
;

GGP
:
	'GGP'
;

BGP
:
	'BGP'
;

TP
:
	'TP'
;

WINDOW
:
	'WINDOW'
;

COLUMNLISTNAME: ((QUESTIONMARK)?CHARACTER) ; // match variable names.
CHARACTER:[a-z]+;
CHAR: (CHARACTER |DIGIT+ );
//COLUMNLISTNAME:(VAR | CHAR ); 

LIT
:
	'\"' [.]+ '\"'
; // match literals

IRI
:
	'<'
	(
		.
	)+? '>'
; // match IRIs

REFERENCE
    : '^^'
 ;

RDFLITERAL
    : ('\"' DECIMAL '\"' (REFERENCE) (LTITYPE)  )
    ;
LTITYPE
    : XSD ':' (FLOATTYPENAME|LONGTYPENAME)
    ;  
XSD: 'xsd'; 
FLOATTYPENAME:'float';
LONGTYPENAME:'long';

DECIMAL
    : DIGIT+ 
    | DIGIT+ DOT DIGIT*
    | DOT DIGIT+
    ;
fragment
DIGIT   :   '0'..'9';  

TYPE
:('String'|'Long'|'Object'|'TripleNestedType'|'TripleBasicElement');
WS
:
	(
		'\t'
		| ' '
		| '\r'
		| '\n'
		| '\u000C'
	)+ -> skip
; // skip spaces, tabs, newlines

LPAREN
:
	'('
;

RPAREN
:
	')'
;

COMMA
:
	','
;

DOT
:
	'.'
;
AND :'AND';
COLON : ':';
AS : 'AS';
WHERE 	: 'WHERE';
GROUPBY : 'GROUPBY';
HAVING 	: 'HAVING';
UNION 	: 'UNION';
SINCE	: 'SINCE';
UNTIL	: 'UNTIL';
REPLACE: 'REPLACE';
FILTER : 'FILTER';
ON	: 'ON';
FACT : 'FACT';
SET : 'SET';
FACTNAME
:	'fact_'DIGIT+ 
;
CACHENAME
:	'cache_'DIGIT+ 
;
OPERATOR : (ASSIGN|COMPARE|PLUS|MINUS|TIMES|DIVIDE|MOD);
COMPARE:
(GREATER|LESS|GREATER_EQUAL|LESS_EQUAL|NEQUAL|EQUAL)
;
QUESTIONMARK:'?';

PLUS
  : '+'
  ;

MINUS
  : '-'
  ;

TIMES
  : '*'
  ;

DIVIDE
  : '/'
  ;

MOD
  : '%'
  ;
ASSIGN
  : '='
  ;
EQUAL
  : '=='
  ;
NEQUAL
  : '!='
  ;
LESS
    : '<'
    ;

UNITTYPE:('SEC'|'MIN'); 
GREATER
    : '>'
    ;
LESS_EQUAL
    : '<='
    ;

GREATER_EQUAL
    : '>='
    ; 

AGGREGATEFUNCTION:(AVG|SUM);
AVG: 'AVG';
SUM: 'SUM'; 