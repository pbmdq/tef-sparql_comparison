package ch.uzh.ifi.ddis.ifp.algebra.parser;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

/**
 * <p>
 * Simple parser test.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
@Test
public class TestParser {
	private static final Logger _log = LoggerFactory
			.getLogger(TestParser.class);

	@Test(dataProvider = "queryprovider", dataProviderClass = ch.uzh.ifi.ddis.ifp.algebra.test.QueryProvider.class)
	@Parameters({ "query" })
	public void test(String query) {

		try {
			InputStream in = new ByteArrayInputStream(query.getBytes());
			CharStream input = new ANTLRInputStream(in);
			FunctionalAlgebraLexer lexer = new FunctionalAlgebraLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			FunctionalAlgebraParser parser = new FunctionalAlgebraParser(tokens);
			parser.query();
		} catch (Exception e) {
			_log.error("Error testing parser", e);
			assert false;
		}
	}

}
