package ch.uzh.ifi.ddis.ifp.algebra.test;

/*
 * #%L
 * Linked Data Esper Algebra
 * %%
 * Copyright (C) 2014 University of Zurich
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.DataProvider;

/**
 * <p>
 * Simple data provider for testing different queries.
 * </p>
 * 
 * @author Thomas Scharrenbach
 * 
 */
public class QueryProvider {

	@DataProvider(name = "queryprovider")
	public static Object[][] provide() throws Exception {
		List<String> queries = new ArrayList<String>();

		// queries.add("FACT(TP(?x, <http://example.com/watches>, ?z )) AS fact_01 WHERE");
		// + "(SINCE(TP(?x, <http://example.com/watches>, ?z )) ");

		//queries.add("PROJECT(?x,BGP(TP(?x, <http://example.com/prop1>, ?m), TP(?x, <http://example.com/prop2>, ?m)))");

		
		//queries.add("GGP(BGP(TP(?x, <http://example.com/prop1>, ?z ))"
		//		+ " BGP(TP(?z, <http://example.com/prop1>, ?m ))) AS cache_01");

		/*
		 queries.add(
		  "BGP(TP(?x, <http://example.com/prop1>, ?z ), TP(?z, <http://example.com/prop2>, ?m)).FILTER(?x = b, ?z = a) AS cache_02"
		  );
		*/
		  
		//queries.add("FACT ( x Object, z Object , sum Long, count Long, end Long, start Long) AS fact_01 ");

		
		//queries.add("FACT (fact_01)"
		//		+ "( UNTIL ON ( cache_01 )  WHERE fact_01.x = cache_01.?x AND fact_01.z = cache_01.?z)");
		
		//queries.add("FACT (fact_01)"
		//		+ "( SINCE ON ( cache_01 ) )");
		
		/*
		queries.add("FACT (fact_01)"
				+ "( MERGE ON ( cache_01 )  WHERE fact_01.z = cache_01.?z"
				+ "SET fact_01.x = cache_01.?x,  fact_01.z = cache_01.?z )");
		*/
		/*
		queries.add("FACT (fact_01)"
				+ "( REPLACE ON ( cache_01 )  WHERE fact_01.z = cache_01.?z"
				+ "SET fact_01.x = texttext, fact_01.count = fact_01.count + 1, fact_01.sum = fact_01.sum + 10)");
		 */

		// cache_01.?x = fact_01.sub AND cache_01.?z = fact_01.obj
		// + "UNION ( UNTIL(TP(?x, <http://example.com/watches>, ?z ))) " );
		// , obj Object, start Long, end Long

		// +
		// "UNION ((REPLACE ?x, ?z ON TP(?x, <http://example.com/watches>, ?z ) ), ?z ) ");

		//queries.add("BGP(TP(?result, <om-owl:floatValue>, ?value ) "
		/*
		queries.add("BGP(TP(?observation,  <om-owl:result>,  ?result) "
				+ ", TP(?observation, <rdf:type>, <weather:TemperatureObservation>) "
				+ ", TP(?sensor, <om-owl:generatedObservation>, ?observation) "
				+ ", TP(?result, <om-owl:uom>, <weather:fahrenheit>) )");
		,TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>)
		*
		*/
		//queries.add("BGP(TP(?observation,  <rdf:type>,   ?sensor))");
		
		//queries.add("BGP(TP(?observation,  <om-owl:procedure>,  ?sensor)).GROUPBY(?sensor)HAVING( AVG ( ?sensor ) <  \"10.0\"^^xsd:float )");
		//HAVING( AVG ( ?value ) >  \"74.0\"^^xsd:float )
		//.WINDOW ( time ( 10 ) ).GROUPBY(?sensor))
		
		//queries.add(" BGP(TP(?observation,  <om-owl:uom>, <weather:fahrenheit>) "
		//		+ ").WINDOW ( time ( 1 ) )");
		
		/*
		queries.add(" PROJECT(?sensor, ?value ,  GGP(BGP(TP(?observation,  <rdf:type>, <weather:WindSpeedObservation>) "
						+ ", TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>) "
						+ ", TP(?observation,  <om-owl:procedure>,  ?sensor)"
						+ ", TP(?observation, <om-owl:result>, ?result)"
						+ ", TP(?result, <om-owl:floatValue>, ?value)"
						+ ").WINDOW ( time ( 10 )).GROUPBY(?sensor)HAVING( AVG ( ?value ) >  \"30.0\"^^xsd:float) "
						+ ")) AS cache_01"
						);
		
		queries.add(" PROJECT(?sensor, ?value ,  GGP(BGP(TP(?observation,  <rdf:type>, <weather:WindSpeedObservation>) "
				+ ", TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>) "
				+ ", TP(?observation,  <om-owl:procedure>,  ?sensor)"
				+ ", TP(?observation, <om-owl:result>, ?result)"
				+ ", TP(?result, <om-owl:floatValue>, ?value)"
				+ ").WINDOW ( time ( 10 )).GROUPBY(?sensor) HAVING( AVG ( ?value ) <  \"30.0\"^^xsd:float)"
				+ ")) AS cache_02"
				);
		
		//HAVING( AVG ( ?value ) <  \"20.0\"^^xsd:float) 
		queries.add("FACT ( sensor Object, value Object , end Long, start Long) AS fact_01 ");

		queries.add("FACT (fact_01) ( SINCE ON ( cache_01 ) WHERE fact_01.?sensor = cache_01.?sensor )");
		
		queries.add("FACT (fact_01) ( UNTIL ON ( cache_02 ) WHERE fact_01.?sensor = cache_02.?sensor)");
			*/
		// VistaTV counting Test n tiple
		queries.add(" BGP("
				+ "  TP(?user, <vistaTV:join>, ?channel) "
				+ ") AS cache_01"
				);
		queries.add(" BGP("
				+ "  TP(?user, <vistaTV:quit>, ?channel) "
				+ ") AS cache_02"
				);
		
		queries.add("FACT ( channel Object, count Long) AS fact_01 ");
		queries.add("FACT (fact_01) ( "
				+ "REPLACE ON ( cache_01 ) "
				+ "WHERE fact_01.?channel != cache_01.?channel "
				+ "SET fact_01.count = \"1\"^^xsd:long AND fact_01.channel = cache_01.?channel )");
		/*
		
		queries.add("FACT ( channel Object, count Long) AS fact_01 ");
		queries.add("FACT (fact_01) ( "
				+ "REPLACE ON ( cache_01 ) "
				+ "WHERE fact_01.?channel != cache_01.?channel "
				+ "SET fact_01.channel = \"1\"^^xsd:long AND fact_01.channel = cache_01.?channel )");
				
				
				
		fact_01.count + \"1\"^^xsd:long + \"1\"^^xsd:long
		
		queries.add("FACT (fact_01) ( "
				+ "REPLACE ON ( cache_01 ) "
				+ "WHERE fact_01.?channel = cache_01.?channel "
				+ "SET fact_01.channel = cache_01.?channel AND fact_01.count = fact_01.count + \"1\"^^xsd:long)");
		
		queries.add("FACT (fact_01) ( "
				+ "REPLACE ON ( cache_01 ) "
				+ "WHERE fact_01.?channel != cache_01.?channel "
				+ "SET fact_01.channel = cache_01.?channel AND fact_01.count = \"0\"^^xsd:long )");
		
		
		queries.add("FACT (fact_01) ( "
				+ "REPLACE ON ( cache_01 ) "
				+ "WHERE fact_01.?channel = cache_01.?channel "
				+ "SET fact_01.channel = cache_01.?channel AND fact_01.count = fact_01.count + \"1\"^^xsd:long)");
		*/
		
		/*
		 * SRbench done face since unitl
		queries.add(" PROJECT(?observation, ?value ,  BGP("
				+ "  TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>) "
				+ ", TP(?observation, <om-owl:floatValue>, ?value)"
				+ ").WINDOW ( time ( 10 )).GROUPBY(?observation)  HAVING( AVG ( ?value ) >  \"40.0\"^^xsd:float) "
				+ ") AS cache_01"
				);
		
		queries.add(" PROJECT(?observation, ?value ,  BGP("
				+ "  TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>) "
				+ ", TP(?observation, <om-owl:floatValue>, ?value)"
				+ ").WINDOW ( time ( 10 )).GROUPBY(?observation) HAVING( AVG ( ?value ) <  \"40.0\"^^xsd:float) "
				+ ") AS cache_02"
				);

		queries.add("FACT ( observation Object, value Object , end Long, start Long) AS fact_01 ");
		queries.add("FACT (fact_01) ( SINCE ON ( cache_01 ) WHERE fact_01.?observation = cache_01.?observation )");
		queries.add("FACT (fact_01) ( UNTIL ON ( cache_02 ) WHERE fact_01.?observation == cache_02.?observation)");
		*/
		
		// project to add window
		//.WINDOW ( time ( 10 ) ).GROUPBY(?sensor)HAVING( AVG ( ?value ) >  \"74.0\"^^xsd:float)
		// since
		// untill
		// merge
		
		/*
		 * 
		 * + ", TP(?temperatureobservation, <om-owl:procedure>, ?sensor)"
						+ ", TP(?temperatureobservation, <rdf:type>, <weather:TemperatureObservation>)"
						+ ", TP(?temperatureobservation, <om-owl:result>, ?temperatureresult)"
						+ ", TP(?temperatureresult, <om-owl:floatValue>, ?tempvalue)"
						
						
		+ ", TP(?temperatureObservation, <rdf:type>, <weather:TemperatureObservation>)"
		+ ", TP(?temperatureObservation, <om-owl:result>, ?temperatureResult)"
		+ ", TP(?temperatureResult, <om-owl:floatValue>, ?tempvalue)"
		*/
		/*HAVING( AVG ( ?tempvalue ) >  \"32.0\"^^xsd:float)
		 * ?windSpeedObservation om-owl:procedure ?sensor ;
                        a weather:WindSpeedObservation ;
                        om-owl:result [ om-owl:floatValue ?windSpeed ]
                        
		 */
		//		queries.add("BGP(TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>) )" );
				
				/*
						+ ", TP(?observation, <om-owl:result>, ?result)"
						+ ", TP(?result, <om-owl:floatValue>, ?value)"
								+ ").GROUPBY(?sensor)");
					*/			
		//queries.add("PROJECT(?sensor, BGP(TP(?observation,  <om-owl:procedure>,  ?sensor),TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>))).GROUPBY(?sensor)");
		
		/*
				+ ", TP(?observation, <om-owl:observedProperty>, <weather:_WindGust>) "
				+ ", TP(?observation, <om-owl:result>, ?result ) "
				+ ", TP(?result, <om-owl:floatValue>, ?value) ).FILTER(?value >= 74.0) AS cache_03");
		*/
		 		
		// queries.add("GGP(BGP(TP(?x, <http://example.com/prop2>, ?z ))) AS cache_02");
		// queries.add("PROJECT(?x,GGP(BGP(TP(?x, <http://example.com/prop>, ?z ))))");
		
		final Object[][] result = new Object[queries.size()][3];

		for (int i = 0; i < result.length; ++i) {
			result[i][0] = queries.get(i);
			result[i][1] = i;
			result[i][2] = result.length;
		}
		return result;
	}
}
