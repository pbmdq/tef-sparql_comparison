The Linked Esper Project.
==============

(C) 2014 University of Zurich, Department of Informatics

This software comes with no warranty.

Licensing
--------------

This software comprises several modules:

- linked-esper-parent (this software)
- linked-esper-core
- linked-esper-sandbox
- linked-esper-algebra

See the file LICENSE for each module to obtain the applicable license.

The modules 'linked-esper-core', and  'linked-esper-sandbox' are licensed 
under the GNU General Public License, version 2  (GPLv2)or any later 
version. Note that under the current licensing terms of Esper, which is 
licensed under GPLv2, any derivative work must be distributed under GPLv2.

The modules 'linked-esper-parent' and 'linked-esper-algebra' are licensed 
under the Apache License, Version 2.0. 

Acknowledgements
--------------

The research leading to these results has received funding 
from the European Union Seventh Framework Programme FP7/2007-2011 
under grant agreement n° 296126.

Summary
--------------
This package integrates TEF-SPARQL into the Esper complex event 
processing engine. 

Dependencies
--------------
except for the linked-esper-algebra module all modules use the following 
other libraries:

- Esper 
- slf4j
- testng (for testing)
- log4j (for testing)

Usage
--------------
TODO